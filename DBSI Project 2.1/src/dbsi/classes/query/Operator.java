package dbsi.classes.query;

import java.util.HashMap;

import dbsi.exception.OperatorException;

public enum Operator {

	EQUALS,
	NOT_EQUALS,
	LESS,
	LESS_EQUALS,
	GREATER,
	GREATER_EQUALS;
	
	private static HashMap<String, Operator> opMap = null;
	private static HashMap<Operator, int[]> operatorComapreResultMap = null;
	
	static {
		opMap = new HashMap<String, Operator>();
		opMap.put("=", Operator.EQUALS);
		opMap.put("<>", Operator.NOT_EQUALS);
		opMap.put("<", Operator.LESS);
		opMap.put("<=", Operator.LESS_EQUALS);
		opMap.put(">", Operator.GREATER);
		opMap.put(">=", Operator.GREATER_EQUALS);
		operatorComapreResultMap = new HashMap<Operator, int[]>();
		operatorComapreResultMap.put(EQUALS, new int[]{0});
		operatorComapreResultMap.put(NOT_EQUALS, new int[]{-1, 1});
		operatorComapreResultMap.put(LESS, new int[]{-1});
		operatorComapreResultMap.put(LESS_EQUALS, new int[]{-1, 0});
		operatorComapreResultMap.put(GREATER, new int[]{1});
		operatorComapreResultMap.put(GREATER_EQUALS, new int[]{0, 1});
	}
	
	public static Operator getOperator(String op) throws OperatorException {
		if(!opMap.keySet().contains(op)) 
			throw new OperatorException("Operator op: " + op + "not supported.");
		return opMap.get(op);
	}
	
	public boolean getCompareResultAllowedForOperator(int compResult) {
		int[] results = operatorComapreResultMap.get(this);
		for(int i : results) {
			if(compResult == i)
				return true;
		}
		return false;
	}
	
	public static Boolean isStringOperator(String arg)
	{
		if(opMap.containsKey(arg))
			return true;
		else
			return false;
	}
	
}
