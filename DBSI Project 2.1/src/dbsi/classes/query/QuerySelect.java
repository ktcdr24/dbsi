package dbsi.classes.query;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Map;

public class QuerySelect extends Query {

	Boolean parametersPresent;
	LinkedHashSet<Integer> projections;
	String exportFilePath;
	protected String importFilePath;
	protected Map<Integer, ArrayList<Condition>> queryConditions;
	protected Map<Integer, ArrayList<Condition>> equalityConditions;

	public Map<Integer, ArrayList<Condition>> getEqualityConditions() {
		return equalityConditions;
	}

	public void setEqualityConditions(
			Map<Integer, ArrayList<Condition>> equalityConditions) {
		this.equalityConditions = equalityConditions;
	}

	public String getExportFilePath() {
		return exportFilePath;
	}

	public void setExportFilePath(String exportFilePath) {
		this.exportFilePath = exportFilePath;
	}

	public LinkedHashSet<Integer> getProjections() {
		return projections;
	}

	public void setProjections(LinkedHashSet<Integer> projections) {
		this.projections = projections;
	}

	public Boolean getParametersPresent() {
		return parametersPresent;
	}

	public void setParametersPresent(Boolean parametersPresent) {
		this.parametersPresent = parametersPresent;
	}

	public QuerySelect(String heapFilePath, QueryType queryType) {
		super(heapFilePath, queryType);
	}

	public String getImportFilePath() {
		return importFilePath;
	}

	public void setImportFilePath(String importFilePath) {
		this.importFilePath = importFilePath;
	}

	public Map<Integer, ArrayList<Condition>> getQueryConditions() {
		return queryConditions;
	}

	public void setQueryConditions(
			Map<Integer, ArrayList<Condition>> queryConditions) {
		this.queryConditions = queryConditions;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[ " + super.toString()).
				append(" [equalityConditions=").append(this.equalityConditions).
				append(" [queryConditions=").append(this.queryConditions).
				append("] ]");
		return sb.toString();
	}

}
