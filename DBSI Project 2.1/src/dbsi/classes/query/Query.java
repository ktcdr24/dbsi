package dbsi.classes.query;


public class Query {

	protected String heapFilePath;
	protected QueryType queryType;
	
	public Query(String heapFilePath, QueryType queryType) {
		this.heapFilePath = heapFilePath;
		this.queryType = queryType;
	}
	
	public String getHeapFilePath() {
		return heapFilePath;
	}
	public void setHeapFilePath(String heapFilePath) {
		this.heapFilePath = heapFilePath;
	}
	public QueryType getQueryType() {
		return queryType;
	}
	public void setQueryType(QueryType queryType) {
		this.queryType = queryType;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Query: [heapFilePath=").append(this.heapFilePath).append("] [queryType=").append(this.queryType).append("]");
		return sb.toString();
	}
	
}
