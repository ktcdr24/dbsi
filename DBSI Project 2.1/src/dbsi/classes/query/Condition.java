package dbsi.classes.query;

public class Condition {

	Operator operator;
	String value;
	
	public Condition() { }
	
	public Condition(Operator op, String val) {
		this.operator = op;
		this.value = val;
	}
	
	public Operator getOperator() {
		return operator;
	}
	public void setOperator(Operator operator) {
		this.operator = operator;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "[Condition [op="+this.operator+"][val="+this.value+"] ]";
	}
	
}
