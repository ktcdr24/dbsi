package dbsi.classes.query;

import java.util.HashSet;
import java.util.Set;

public class QueryBuildHash extends Query {

	public QueryBuildHash(String heapFilePath, QueryType queryType) {
		super(heapFilePath, queryType);
		this.columnIds = new HashSet<Integer>();
	}
	
	protected Set<Integer> columnIds;

	public Set<Integer> getColumnIds() {
		return columnIds;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[ " + super.toString()).append(" [columnId=").append(this.columnIds).append("] ]");
		return sb.toString();
	}

}
