package dbsi.classes.query;

import java.util.Set;
import java.util.TreeSet;

public class QueryInsert extends Query {
	
	public QueryInsert(String heapFilePath, QueryType queryType) {
		super(heapFilePath, queryType);
		this.hashColumnIds = new TreeSet<Integer>();
	}

	protected String importFilePath;
	
	protected Set<Integer> hashColumnIds;

	public String getImportFilePath() {
		return importFilePath;
	}
	public void setImportFilePath(String importFilePath) {
		this.importFilePath = importFilePath;
	}
	public Set<Integer> getHashColumnIds() {
		return hashColumnIds;
	}
	public void setHashColumnIds(Set<Integer> hashColumnId) {
		this.hashColumnIds = hashColumnId;
	}
	
	public boolean isHahSUpported() {
		return !(this.hashColumnIds == null);
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[ " + super.toString()).append(" [importFilePath=").append(this.importFilePath).
				append("][HashColumn=").append(this.hashColumnIds).append("] ]");
		return sb.toString();
	}
	
}
