package dbsi.classes;

import java.util.HashMap;
import java.util.Set;

import dbsi.exception.CursorException;

public class CursorRecord {
	
	HashMap<Integer, String> recordFieldsMap = new HashMap<Integer, String>();
	
	public void setField(int index, String value) {
		recordFieldsMap.put(index, value);
	}
	
	public String getField(int index) throws CursorException {
		if(!recordFieldsMap.keySet().contains(index))
			throw new CursorException("Record dosen not have an field with index " 
					+ index + ". (Please note that index starts at 1)");
		return recordFieldsMap.get(index);
	}
	
	public String getRecId() throws CursorException {
		if(!recordFieldsMap.keySet().contains(0)) 
			throw new CursorException("Record dosen not have an field with index 0. (RecId is not present)");
		return recordFieldsMap.get(0);
	}
	
	/**
	 * return the CursorRecord as String. 
	 * if isRIdNeeded == true, RId will be added the to string.
	 * otherwise, it will not be added.
	 * @param isRIdNeeded
	 * @return
	 */
	public String getRecordAsString(boolean isRIdNeeded) {
		StringBuffer sb = new StringBuffer();
		Set<Integer> keys = recordFieldsMap.keySet();
		for(Integer k : keys) {
			if(k==0) {
				if(isRIdNeeded)	
					sb.append(recordFieldsMap.get(k).trim()).append(",");
			} else { 
				sb.append(recordFieldsMap.get(k).trim()).append(",");
			}
		}
		sb.setLength(sb.length() - 1);
		return sb.toString();
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		Set<Integer> keys = recordFieldsMap.keySet();
		sb.append("{");
		for(Integer k : keys)
			sb.append(k).append(":").append(recordFieldsMap.get(k)).append(",");
		sb.setLength(sb.length() - 1);
		sb.append("}");
		return sb.toString();
	}

}
