package dbsi.classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;
import java.util.Set;
import java.util.TreeSet;

import dbsi.Main;
import dbsi.exception.BucketException;
import dbsi.exception.HashException;
import dbsi.exception.InvalidFileException;
import dbsi.exception.OverflowException;
import dbsi.helper.ByteHelper;
import dbsi.helper.HashFileHelper;
import dbsi.type.Bucket;
import dbsi.type.TypeFormat;

public class HashFile {
	
	public static final Integer INITIAL_SIZE = 4;
	public static final Short HEADER_LENGTH = 8 + 8 + 4;
	
	private File hashFile = null;
	private Long nextPtrIndex = null;
	private Long size = null;
	private Integer colLength = null;
	private TypeFormat colFormat = null;
	private OverflowFile overflowFile = null;
	
	/**
	 * 
	 * @param path
	 * @param toCreate - if files needed to be created
	 * @param colLength
	 * @param colFormat
	 * @throws InvalidFileException
	 */
	public HashFile(String path, boolean toCreate, int colLength, TypeFormat colFormat) throws InvalidFileException {
		try {
			if (toCreate) {
				this.hashFile = HashFileHelper.createNewHashFile(path, colLength, colFormat);
				this.size = INITIAL_SIZE.longValue();
			} else {
				this.hashFile = HashFileHelper.getHashFile(path);
			}
			this.overflowFile = new OverflowFile(this.hashFile.getAbsolutePath(), toCreate, colLength, colFormat);
			this.colFormat = colFormat;
			readAttributesFromHeader();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Creats an instance of HashFile from existing hash file.
	 * @param path
	 * @param format
	 */
	public HashFile(String path, TypeFormat colFormat) {
		try {
			this.hashFile = HashFileHelper.getHashFile(path);
			this.overflowFile = new OverflowFile(this.hashFile.getAbsolutePath(), false, colLength, colFormat);
			readAttributesFromHeader();
			this.colFormat = colFormat;
		} catch (InvalidFileException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * read all the attributes from header and store it in the class members.
	 */
	private void readAttributesFromHeader() {
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(hashFile, "r");
			byte[] readByte = new byte[8];
			// read nextPtr
			raf.read(readByte);
			this.nextPtrIndex = ByteHelper.rawToI8(readByte, 0);
			// read size
			readByte = new byte[8];
			raf.read(readByte);
			this.size = ByteHelper.rawToI8(readByte, 0);
			// read Column Length
			readByte = new byte[4];
			raf.read(readByte);
			this.colLength = ByteHelper.rawToI4(readByte, 0);			
			if(Main.DEBUG) System.out.println(">>> ??? readAttributesFromHeader() nextPtrIndex="+this.nextPtrIndex +
					", size="+this.size +
					", colLength="+this.colLength);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	private void incrementNextPointer() {
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(hashFile, "rw");
			// read nextPtr
			byte[] readByte = new byte[8];
			raf.read(readByte);
			long fileNextPtrIndex = ByteHelper.rawToI8(readByte, 0);
			raf.read(readByte);
			long fileSize = ByteHelper.rawToI8(readByte, 0);
			if(this.nextPtrIndex.longValue() != fileNextPtrIndex) 
				throw new HashException("HashFile is out of sync. this.nextPtrIndex=" + this.nextPtrIndex + 
						", but in HashFile the nextPointerIndex=" + fileNextPtrIndex);
			else if(this.size.longValue() != fileSize) 
				throw new HashException("HashFile is out of sync. this.size=" + this.size + 
						", but in HashFile the size=" + fileSize);
			else { 
				raf.seek(0);
				this.nextPtrIndex++;
				if(this.nextPtrIndex.longValue() == this.size.longValue() + 1) {
					if(Main.DEBUG) System.out.println("uuu Doubling the direcotry Size");
					// if nextPtr == size 
					// then double the size and initialize nextPtr to start
					this.nextPtrIndex = (long)1;				
					if(Main.DEBUG) System.out.println(">>> uuu Writing updated NextPtrIndex=" + this.nextPtrIndex);
					raf.write(ByteHelper.i8ToRaw(this.nextPtrIndex.longValue()));
					this.size *= 2;
					if(Main.DEBUG) System.out.println(">>> uuu Writing updated size=" + this.size);
					raf.write(ByteHelper.i8ToRaw(this.size.longValue()));
				} else {
					if(Main.DEBUG) System.out.println(">>> uuu Writing updated NextPtrIndex=" + this.nextPtrIndex);
					raf.write(ByteHelper.i8ToRaw(this.nextPtrIndex.longValue()));
				}
			}				
			if(Main.DEBUG) System.out.println(">>> ??? incrementNextPointer() nextPtrIndex="+this.nextPtrIndex +
					", size="+this.size +
					", colLength="+this.colLength);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (HashException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * gets the Bucket present at the specified hashIndex (00, 01, 10, 11, 100...)
	 * @param hashIndex
	 * @return
	 */
	private Bucket getBuckteAtIndex(Long hashIndex) {
		if(Main.DEBUG) System.out.println(">>> trying to get bucket at index = " + hashIndex);
		RandomAccessFile raf = null;
		Bucket bucketAtIndex = new Bucket(this.colLength, this.colFormat);
		try{
			raf = new RandomAccessFile(hashFile, "r");
			long toSeek = HEADER_LENGTH + ((hashIndex - 1)  * Bucket.getBucketSize(this.colLength));
			if(Main.DEBUG) System.out.println(">>> hash header len = " + HEADER_LENGTH + ", Seeking = " + toSeek);
			raf.seek(toSeek);
			byte[] b = bucketAtIndex.getBytes();
			raf.read(b);
			bucketAtIndex.setBytes(b);
			return bucketAtIndex;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (BucketException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}
	
	/**
	 * insert/replace the bucket at the specified hashIndex. 
	 * @param bucket
	 * @param hashIndex
	 */
	private void putBuckteAtIndex(Bucket bucket, Long hashIndex) {
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(hashFile, "rw");
			long toSeek = HEADER_LENGTH + ((hashIndex - 1) * Bucket.getBucketSize(this.colLength));
			raf.seek(toSeek);
			raf.write(bucket.getBytes());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	public void freeBucketAtIndex(Long hashIndex) {
		RandomAccessFile raf = null;
		Bucket bucket= new Bucket(this.colLength, this.colFormat);
		byte[] b = null;
		try{
			if(Main.DEBUG) System.out.println(">>> --- HashFile Freeing Bucket at index = " + hashIndex);
			raf = new RandomAccessFile(hashFile, "rw");
			long toSeek = HEADER_LENGTH + ((hashIndex - 1) * Bucket.getBucketSize(this.colLength));
			raf.seek(toSeek);
			b = bucket.getBytes();
			raf.read(b);
			bucket.setBytes(b);
			// write empty bucket - free bucket
			raf.seek(toSeek);
			b = new byte[bucket.getBytes().length];
			raf.write(b);
			// move to next linked bucket
			Long overflowPointer = bucket.getPointer();
			if(Main.DEBUG) System.out.println(">>> --- HashFile Freed Bucket overflow pointer = " + overflowPointer);
			if(overflowPointer == null || overflowPointer.longValue() == 0)
				return;
			else 	
				this.overflowFile.freeLinkedBucketsAtIndex(overflowPointer);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (BucketException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * Adds the given HashPair to the HashFile at the location indicated by the 
	 * hash value. 
	 * @param hash
	 * @param hashPair
	 * @param isPrimary
	 */
	public void addHashPair(Long hash, HashPair hashPair, boolean isPrimary) {
		//long nextPtrIndex = (this.nextPtrIndex - this.headerLength) / Bucket.getBucketSize(this.colLength);
		if(Main.DEBUG) System.out.println(">>> getIndexForHash => hash="+hash+", nextPtrIndex="+this.nextPtrIndex+", size="+this.size);
		Long hashIndex = HashFileHelper.getIndexForHash(hash, this.nextPtrIndex, this.size);
		if(Main.DEBUG) System.out.println(">>> HashIndex = " + hashIndex);
		Bucket bucket = null;
		bucket = getBuckteAtIndex(hashIndex);
		if(Main.DEBUG) System.out.println(">>> hf1 bucket - " + bucket);
		Boolean isSplitNeeded = bucket.putHashPair(hashPair, this.overflowFile);
		if(Main.DEBUG) System.out.println(">>> hf2 bucket - " + bucket);
		putBuckteAtIndex(bucket, hashIndex);
				
		if(isPrimary && isSplitNeeded == null) {
			System.err.println("Shouldn't be reached");
			System.exit(0);
		} else if (isPrimary && isSplitNeeded == true){
			splitBucket();
		}
	}
	
	private void splitBucket() {
		//long nextPtrIndex = (this.nextPtrIndex - this.headerLength) / Bucket.getBucketSize(this.colLength);
		//Long hashIndex = HashFileHelper.getIndexForHash(hash, this.nextPtrIndex, this.size);
		if(Main.DEBUG) System.out.println(">>> Splitting Bucket with nextPtrIndex = " + this.nextPtrIndex);
		
		Long bucketToBeSplitHashIndex = this.nextPtrIndex;
		Bucket bucketToBeSplit = getBuckteAtIndex(bucketToBeSplitHashIndex);
		// get all hash paris of the bucketToBeSplit
		LinkedList<HashPair> btbsHashPairs = bucketToBeSplit.getAllHashPairs(this.overflowFile);
		if(Main.DEBUG) System.out.println(">>> BucketToBeSplit - HashPairs = " + btbsHashPairs);
		// free the bucketToBeSplit
		freeBucketAtIndex(bucketToBeSplitHashIndex);
		
		// create new bucket
		Long newBucketHashIndex = this.nextPtrIndex + this.size;
		if(Main.DEBUG) System.out.println(">>> New Bucket HashIndex = " + newBucketHashIndex);
		if(Main.DEBUG) System.out.println(">>> Adding new Bucket to the HashFile at HashIndex = " + newBucketHashIndex);
		// write new bucket to HashFile
		putBuckteAtIndex(new Bucket(this.colLength, this.colFormat), newBucketHashIndex);
		
//		Bucket newBucket = getBuckteAtIndex(newBucketHashIndex);
//		// get all HashPairs of the new bucket - this should be empty.
//		LinkedList<HashPair> nbHashPairs = newBucket.getAllHashPairs(this.overflowFile);
//		if(Main.DEBUG) System.out.println(">>> NewBucket - HashPairs = " + nbHashPairs);

		incrementNextPointer();
		
		// split the HashPairs 
		splitHashPairs(btbsHashPairs, bucketToBeSplitHashIndex, newBucketHashIndex);
	}
	
	private void splitHashPairs(LinkedList<HashPair> btbsHashPairs, Long bucketToBeSplitHashIndex, Long newBucketHashIndex) {
		Long newHashIndex = null;
		for(HashPair hp1 : btbsHashPairs) {
			newHashIndex = HashFileHelper.getIndexForHash(this.colFormat.getHashForValue(hp1.getValue()), this.nextPtrIndex, this.size);
			if(newHashIndex.longValue() == bucketToBeSplitHashIndex.longValue())
				if(Main.DEBUG) System.out.println(">>> +++ HashPair="+hp1+" goes to OLD bucket at index="+newHashIndex);
			else 
				if(Main.DEBUG) System.out.println(">>> +++ HashPair="+hp1+" goes to NEW bucket at index="+newHashIndex);
			addHashPair(this.colFormat.getHashForValue(hp1.getValue()), hp1, false);
		}
	}
	
	/**
	 * get the File Descriptor
	 * @return
	 */
	public File getHashFileDescriptor() {
		return this.hashFile;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[HashFile [name=").append(this.hashFile.getName()).
				append("][ColLength=").append(this.colLength).
				append("][ColFormat=").append(this.colFormat).
				append("]").append(this.overflowFile).append(" ]");
		return sb.toString();
	}
	
	public void printDebug() {
		System.out.println("--- HashFile Contents");
		System.out.println(this);
		RandomAccessFile raf = null;
		
		try{
			raf = new RandomAccessFile(hashFile, "r");
			raf.seek(HEADER_LENGTH);
			Bucket bucketAtIndex = null;
			byte[] bucketByte = new byte[Bucket.getBucketSize(this.colLength)];
			int index = 1;
			while(raf.read(bucketByte) != -1) {
				bucketAtIndex = new Bucket(this.colLength, this.colFormat);
				bucketAtIndex.setBytes(bucketByte);
				System.out.print("  HashIndex = " + index + " == ");
//				System.out.println("  bucket: " + bucketAtIndex);
				bucketAtIndex.printDebug(this.overflowFile);
				index++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (BucketException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		System.out.println("--- End HashFile Contents \n");		
	}
	
	public Set<Long> getIndexedRecords(Long hash, String value)
	{
//		Long hashIndex = this.getHashIndex(hash);
		Long hashIndex = HashFileHelper.getIndexForHash(hash, this.nextPtrIndex, this.size);
		Bucket bucket = this.getBuckteAtIndex(hashIndex);
		Set<Long> recs = new TreeSet<Long>();
		while(true)
		{
			for( int i=0;i<Bucket.RECORDS_PER_BUCKET;++i)
			{
				try {
						recs.add(bucket.getHashPair(i).getRecId());
				} catch (BucketException e) {
					e.printStackTrace();
				}
			}
			try {
				if(bucket.getPointer()!=0)
					bucket=this.overflowFile.getBucket(bucket.getPointer(), false);
				else
					break;
			} catch (OverflowException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return recs;
	}
	
}
