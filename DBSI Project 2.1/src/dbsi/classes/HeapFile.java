package dbsi.classes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import dbsi.Main;
import dbsi.classes.query.Condition;
import dbsi.exception.HashException;
import dbsi.exception.InvalidFileException;
import dbsi.exception.SchemaException;
import dbsi.helper.ByteHelper;
import dbsi.helper.HeapFileHelper;

public class HeapFile {
	
	private static int HEADER_SIZE_INDEX = 0;
	private static int HEADER_SIZE_LENGTH = 8;
	private static int NUM_RECORDS_INDEX = 8;
	private static int NUM_RECORDS_LENGTH = 8;
	private static int REC_SIZE_INDEX = 16;
	private static int REC_SIZE_LENGTH = 8;
	private static int SCHEMA_INDEX = 24;
	
	private File heap;
	private Schema schema;
	private long headerSize;
	private long numRecords;
	private long recordLength;
	private Map<Integer, HashFile> columnIdToHashFileMap;
	
	/**
	 * creates new heap file(with header) if the HeapFile is not already present.
	 * if HeapFile exists, and if the HeapFile's schema is same as the given schema, returns that HeapFile,
	 * else throws SchemaException.
	 * @param path
	 * @param csvSchemaString
	 * @throws SchemaException
	 */
	public HeapFile(String path, String csvSchemaString) throws SchemaException {
		this.schema = new Schema(csvSchemaString, false);
		try{
			if(HeapFileHelper.isHeapFilePresent(path)) {
				this.heap = HeapFileHelper.getHeapFile(path);
				this.headerSize = this.getHeaderSizeFromHeader();
				if(! (isSchemaSame(getSchemaStringFromHeader())) ) {
					throw new SchemaException("Heap File schema is incompatabile with given schema(" + csvSchemaString + ")");
				}
				if(Main.VERBOSE) System.out.println("Existing HeapFile is found with the same schema.");
				this.columnIdToHashFileMap = generateColumnIdToHashFileMap(false);
			} else {
				if(Main.VERBOSE) System.out.println("Creating a new HeapFile: " + path);
				this.heap = HeapFileHelper.createHeapFile(path, this.schema);
				this.headerSize = this.getHeaderSizeFromHeader();
				this.columnIdToHashFileMap = generateColumnIdToHashFileMap(true);
			}
			this.numRecords = this.getNumRecordsFromHeader();
			this.recordLength = this.getRecordLengthFromHeader();
		} catch(InvalidFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (HashException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * gets the existing HeapFile from the given path
	 * @param path
	 */
	public HeapFile(String path) {
		try{
			this.heap = HeapFileHelper.getHeapFile(path);
			this.headerSize = this.getHeaderSizeFromHeader();
			this.numRecords = this.getNumRecordsFromHeader();
			this.recordLength = this.getRecordLengthFromHeader();
			this.schema = new Schema(getSchemaStringFromHeader(), true);
			this.columnIdToHashFileMap = generateColumnIdToHashFileMap(false);
		} catch(InvalidFileException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (HashException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * helper method for reading the header size from the Header of the this instance's HeapFile
	 * @return
	 * @throws IOException
	 */
	private long getHeaderSizeFromHeader() throws IOException {
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(heap, "r");
			byte[] headerSize = new byte[HEADER_SIZE_LENGTH];
			raf.seek(HEADER_SIZE_INDEX);
			raf.readFully(headerSize, 0, HEADER_SIZE_LENGTH);
			return ByteHelper.rawToI8(headerSize, 0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(raf != null) raf.close();
		}
		return -1;
	}
	
	/**
	 * helper method for reading the Number of Records from the Header of the this instance's HeapFile
	 * @return
	 * @throws IOException
	 */
	private long getNumRecordsFromHeader() throws IOException {
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(heap, "r");
			byte[] numRecs = new byte[NUM_RECORDS_LENGTH];
			raf.seek(NUM_RECORDS_INDEX);
			raf.readFully(numRecs, 0, NUM_RECORDS_LENGTH);
			return ByteHelper.rawToI8(numRecs, 0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(raf != null) raf.close();
		}
		return -1;
	}
	
	/**
	 * helper method for reading the Record Length from the Header of the this instance's HeapFile
	 * @return
	 * @throws IOException
	 */
	private long getRecordLengthFromHeader() throws IOException {
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(heap, "r");
			byte[] recLength = new byte[REC_SIZE_LENGTH];
			raf.seek(REC_SIZE_INDEX);
			raf.readFully(recLength, 0, REC_SIZE_LENGTH);
			return ByteHelper.rawToI8(recLength, 0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(raf != null) raf.close();
		}
		return -1;
	}
	
	/**
	 * helper method for reading the Schema String from the Header of the this instance's HeapFile
	 * @return
	 * @throws IOException
	 */
	private String getSchemaStringFromHeader() throws IOException {
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(heap, "r");
			raf.seek(SCHEMA_INDEX);
			byte[] slbyte = new byte[4];
			raf.read(slbyte);
			int schemaLength = ByteHelper.rawToI4(slbyte, 0);
			byte[] sbyte = new byte[schemaLength];
			raf.read(sbyte);
			return new String(ByteHelper.rawToCx(sbyte));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(raf != null) raf.close();
		}
		return null;
	}
	
	/**
	 * returns the Number of Columns which have Hash Indexes. 
	 * @return
	 * @throws IOException
	 */
	private short getNumOfHashColumnsFromHeader() throws IOException {
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(heap, "r");
			raf.seek(SCHEMA_INDEX);
			byte[] slbyte = new byte[4];
			raf.read(slbyte);
			int schemaLength = ByteHelper.rawToI4(slbyte, 0);
			raf.seek(SCHEMA_INDEX + 4 + schemaLength);
			byte[] sbyte = new byte[2];
			raf.read(sbyte, 0, 2);
			return (short)ByteHelper.rawToI2(sbyte, 0);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(raf != null) raf.close();
		}
		return -1;
	}
	
	/**
	 * returns the List of ColumnID's with Hash Indexes.
	 * @return
	 * @throws IOException
	 */
	private List<Integer> getHashColumnsFromHeader() throws IOException {
		List<Integer> hashColumnIds = new ArrayList<Integer>();
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(heap, "r");
			raf.seek(SCHEMA_INDEX);
			byte[] slbyte = new byte[4];
			raf.read(slbyte);
			int schemaLength = ByteHelper.rawToI4(slbyte, 0);
			raf.seek(SCHEMA_INDEX + 4 + schemaLength);
			byte[] sbyte = new byte[2];
			raf.read(sbyte, 0, 2);
			short numHashColumns = (short)ByteHelper.rawToI2(sbyte, 0);
			byte[] colIdByte = null; 
			for(int i=0; i<numHashColumns; ++i) {
				colIdByte = new byte[2];
				raf.read(colIdByte, 0, 2);
				hashColumnIds.add(ByteHelper.rawToI2(colIdByte, 0));
			}
			return hashColumnIds;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally{
			if(raf != null) raf.close();
		}
		return null;
	}
	
	/**
	 * returns the Map of ColumnID's with Hash Indexes and corresponding Hash Files 
	 * @return
	 * @throws HashException
	 * @throws InvalidFileException 
	 */
	private Map<Integer, HashFile> generateColumnIdToHashFileMap(boolean isCreateHashFiles) throws HashException, InvalidFileException {
		List<Integer> hashColIds = null;
		Map<Integer, HashFile> colIdToHashFileMap = new HashMap<Integer, HashFile>();
		try {
			hashColIds = getHashColumnsFromHeader();
			for(Integer s : hashColIds) {
				if(colIdToHashFileMap.containsKey(s)) {
					throw new HashException("Duplicate Hash Column ID ("+s+") in the heap file header.");
				}
				String path = heap.getParent() + "/" + this.heap.getName() + ".col" + s + ".hash";
				int lengthOfColumnForHashing = this.schema.getFieldLength(s.intValue());
				HashFile f = new HashFile(path , isCreateHashFiles, lengthOfColumnForHashing, this.schema.getFieldType(s.intValue()));
				colIdToHashFileMap.put(s, f);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return colIdToHashFileMap;
	}
	
	/**
	 * adds a ColumnID on which the Hash Index is newly built to the header 
	 * @param colID
	 * @throws IOException
	 * @throws HashException
	 * @throws InvalidFileException
	 */
	public void addHashColumnIdToHeader(Set<Integer> colIDs) throws IOException, HashException {
		for(int colID: colIDs) {
			// if coldID is already present in the columnIdToHashFileMap, then its a duplicate.
			if(this.columnIdToHashFileMap.containsKey(colID)) {
				throw new HashException("Duplicate Hash Column ID ("+colID+") in the heap file header.");
			}
		}
		short numHashCols = getNumOfHashColumnsFromHeader();
		RandomAccessFile raf = null;
		try{
			raf = new RandomAccessFile(heap, "rw");
			raf.seek(SCHEMA_INDEX);
			byte[] slbyte = new byte[4];
			raf.read(slbyte);
			int schemaLength = ByteHelper.rawToI4(slbyte, 0);
			int readTill = SCHEMA_INDEX
					+ 4 /*schema length in header*/ 
					+ schemaLength;
			raf.seek(0);
			// skip till schema.
			raf.skipBytes(readTill);
			// modify NumHashCols = NumHashCols + 1 
			raf.write(ByteHelper.i2ToRaw(numHashCols + colIDs.size()));
			// skip original HasHColID's list
			raf.skipBytes(2*numHashCols);
			
			for(int colID: colIDs) {
				// write new HashColID
				raf.write(ByteHelper.i2ToRaw(colID));
				// create new HashFile and add it to map
				String path = heap.getParent() + "/" + this.heap.getName() + ".col" + colID + ".hash";
				int lengthOfColumnForHashing = this.schema.getFieldLength(colID);
				HashFile hashFile = new HashFile(path, true, lengthOfColumnForHashing, this.schema.getFieldType(colID));
				this.columnIdToHashFileMap.put(colID, hashFile);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFileException e) {
			e.printStackTrace();
		} finally {
			if(raf != null) raf.close();
		}
	}
	
	/**
	 * Builds HashIndex on Existing Column specified by colID
	 * @param colID
	 * @throws HashException
	 */
	public void buildHashOnColumn(Set<Integer> colIDs) throws HashException {
		for(int colID: colIDs) {
			if(!this.columnIdToHashFileMap.keySet().contains(colID))
				throw new HashException("Attempting to Build Hash on colum:"+colID+", but HashColumn not found in Header!!!!");
			if(Main.VERBOSE) System.out.println("Building Hash Index on Column: " + colID);
		}
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(heap, "rw");
			raf.seek(this.headerSize + 1);			
			byte[] record = new byte[(int) this.recordLength];
			Long recID = null;
			String value = null;
			Long hash = null;
			HashPair hashPair = null;
			for(int i=0; i<this.numRecords; ++i) {
				if(Main.VERBOSE && i%1000 == 0) System.out.println("Processing record ID: " + i + " now. Please Wait");
				raf.read(record);
				recID = ByteHelper.rawToI8(record, 0);
				for(int colID: colIDs) {					 
					value = this.schema.getColumnValue(record, colID);
					hashPair = new HashPair(value, recID);
					hash = this.schema.getFieldType(colID).getHashForValue(value);
					if(Main.DEBUG) System.out.println("\n> try to add " + hash + ", " + hashPair + " to HashFile");
					columnIdToHashFileMap.get(colID).addHashPair(hash, hashPair, true);
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SchemaException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * Adds the given record to the end of the heap file.
	 * @param record
	 * @throws SchemaException
	 */
	public void addRecord(String record) throws SchemaException {
		RandomAccessFile raf = null;
		long tempNumRecords = this.numRecords;
		try {
			raf = new RandomAccessFile(heap, "rw");
			long recID = this.headerSize + (tempNumRecords * this.recordLength) + 1;
			record = recID + "," + record;
			byte[] recByte = this.schema.recordToBytes(record);
			raf.seek(recID);
			raf.write(recByte);			
			tempNumRecords++;
			byte[] numRecs = ByteHelper.i8ToRaw(tempNumRecords);
			raf.seek(NUM_RECORDS_INDEX);
			raf.write(numRecs);
			this.numRecords = tempNumRecords;
			// update index
			Set<Integer> colIDs = columnIdToHashFileMap.keySet();
			String value = null;
			Long hash = null;
			HashPair hashPair = null;
			for(int colID: colIDs) {					 
				value = this.schema.getColumnValue(recByte, colID);
				hashPair = new HashPair(value, recID);
				hash = this.schema.getFieldType(colID).getHashForValue(value);
				if(Main.DEBUG) System.out.println("\n> try to add " + hash + ", " + hashPair + " to HashFile");
				columnIdToHashFileMap.get(colID).addHashPair(hash, hashPair, true);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	/**
	 * adds all the given records to the end of this heap file
	 * @param recordStrings
	 * @throws SchemaException
	 */
	public void addRecords(List<String> recordStrings) throws SchemaException {
		for(String recStr : recordStrings) {
			addRecord(recStr);
		}
	}
	
	/**
	 * return all the records in the form of a Cursor
	 * @return
	 */
	public Cursor getAllRecords() {
		Set<Long> recs = new LinkedHashSet<Long>();
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(heap, "rw");
			raf.seek(this.headerSize + 1);			
			byte[] record = new byte[(int) this.recordLength];
			for(int i=0; i<this.numRecords; ++i) {
				raf.read(record);
				recs.add(ByteHelper.rawToI8(record, 0));
			}
			Cursor cursor = new Cursor();
			cursor.addAllRecords(recs);
			cursor.setSchema(this.schema.getSchemaString());
			cursor.setProjectionIndexes(new HashSet<Integer>());
			return cursor;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}
	
	/**
	 * returns all the projected fields of all the records which pass the given selection criteria. 
	 * @param indexedEqualitySelections
	 * @param selections
	 * @param projectionIndexes
	 * @return
	 */
	public Cursor getQualifiyingRecords(Map<Integer, ArrayList<Condition>> indexedEqualitySelections, 
										Map<Integer, ArrayList<Condition>> selections, 
										Set<Integer> projectionIndexes) {
		if(indexedEqualitySelections!=null && indexedEqualitySelections.size()>0) {
			Set<Long> setOfRecords = new TreeSet<Long>();
			Cursor cursor = new Cursor();
			String schemaString = this.schema.getSchemaString();
			cursor.setSchema(schemaString);
			cursor.setProjectionIndexes(projectionIndexes);
			for(Integer parameter : indexedEqualitySelections.keySet())
			{
				if(indexedEqualitySelections.get(parameter).size() > 1) {
					cursor.addAllRecords(new LinkedHashSet<Long>());
					return cursor;
				} else {
					String value = indexedEqualitySelections.get(parameter).get(0).getValue();
					HashFile hashFile = this.columnIdToHashFileMap.get(parameter);
					Long hash = this.schema.getFieldType(parameter).getHashForValue(value);					
					Set<Long> currentParametetSetOfRecords = hashFile.getIndexedRecords(hash, value);
					RandomAccessFile raf1 = null;
					Set<Long> recs = new LinkedHashSet<Long>();
					try {
						raf1 = new RandomAccessFile(heap, "r");
						for(Long recordId : currentParametetSetOfRecords) {
							raf1.seek(recordId);			
							byte[] record = new byte[(int) this.recordLength];
								raf1.read(record);
								if(indexedEqualitySelections == null || indexedEqualitySelections.isEmpty()) {
									recs.add(ByteHelper.rawToI8(record, 0));
								} else { 
									// if record passes the selection condition, add it to recs, otherwise dont add it
									if(this.schema.isSelectionPassed(record, indexedEqualitySelections))  
										recs.add(ByteHelper.rawToI8(record, 0));
								}
						}
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} finally {
						if(raf1 != null)
							try {
								raf1.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
					}
					
					if(recs.size()==0) {
						cursor.addAllRecords(new LinkedHashSet<Long>());
						return cursor;
					} else {
						if(setOfRecords.size()==0) {
							setOfRecords=recs;
						} else {
							if(java.util.Collections.disjoint(setOfRecords, recs)) {
								cursor.addAllRecords(new LinkedHashSet<Long>());
								return cursor;
							} else 
								setOfRecords.retainAll(recs);
						}
					}
				}
			}
			RandomAccessFile raf = null;
			Set<Long> recs = new LinkedHashSet<Long>();
			try {
				raf = new RandomAccessFile(heap, "r");
				for(Long recordId : setOfRecords) {
					raf.seek(recordId);			
					byte[] record = new byte[(int) this.recordLength];
						raf.read(record);
						if(selections == null || selections.isEmpty()) {
							recs.add(ByteHelper.rawToI8(record, 0));
						} else { 
							// if record passes the selection condition, add it to recs, otherwise dont add it
							if(this.schema.isSelectionPassed(record, selections))  
								recs.add(ByteHelper.rawToI8(record, 0));
						}
				}
				cursor.addAllRecords(recs);
				return cursor;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(raf != null)
					try {
						raf.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			return null;
		} else {
			Set<Long> recs = new LinkedHashSet<Long>();
			RandomAccessFile raf = null;
			try {
				raf = new RandomAccessFile(heap, "rw");
				raf.seek(this.headerSize + 1);			
				byte[] record = new byte[(int) this.recordLength];
				for(int i=0; i<this.numRecords; ++i) {
					raf.read(record);
//					System.out.println("-recordString-"+ByteHelper.rawToCx(record, 0, (int)this.recordLength));
					if(selections == null || selections.isEmpty()) {
						recs.add(ByteHelper.rawToI8(record, 0));
					} else { 
						// if record passes the selection condition, add it to recs, otherwise dont add it
						if(this.schema.isSelectionPassed(record, selections))  
							recs.add(ByteHelper.rawToI8(record, 0));
					}
				}
				Cursor cursor = new Cursor();
				cursor.addAllRecords(recs);
				String schemaString = this.schema.getSchemaString();
				cursor.setSchema(schemaString);
				cursor.setProjectionIndexes(projectionIndexes);
				return cursor;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				if(raf != null)
					try {
						raf.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
			}
			return null;
		}

	}
	
	/**
	 * read the record associated with the RecID
	 * @param recID
	 * @param projectionIndexes
	 * @return
	 */
	public CursorRecord getCursorRecord(Long recID, Set<Integer> projectionIndexes) {
		RandomAccessFile raf = null;
		try {
			raf = new RandomAccessFile(heap, "rw");
			raf.seek(recID);
			byte[] record = new byte[(int)this.recordLength];
			raf.read(record);
			return this.schema.recordToCursorRecord(record, projectionIndexes);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(raf != null)
				try {
					raf.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
		return null;
	}
	
	/**
	 * Checks if this HeapFile's schema is same as given Schema string
	 * @param schemaString
	 * @return
	 */
	public boolean isSchemaSame(String schemaString) {
		return this.schema.equals(new Schema(schemaString, true));
	}
	
	/**
	 * returns the schema of the HeapFile.
	 * @return
	 */
	public String getSchemaAsString() {
		return this.schema.getSchemaString();
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		try {
			sb.append("[HEAP [filepath=").append(this.heap.getCanonicalPath()).
					append("][headersize=").append(this.headerSize).
					append("][numrec=").append(this.numRecords).
					append("][recsize=").append(this.recordLength).
					append("]").append(this.schema).
					append("[hashCols:").append(this.columnIdToHashFileMap.keySet()).append("] ]");
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}
	
	public void printDebug() {
		System.out.println("-------- HEAP FILE -----------");
		System.out.println(this);
		Cursor c = getAllRecords();
		Iterator<Long> iter = c.iterator();
		Long recID = null;
		Set<Integer> projectionIndexes = c.getProjectionIndexes();
		System.out.println("------ Records");
        while(iter.hasNext())
        {
        	recID = iter.next();
        	System.out.println(getCursorRecord(recID, projectionIndexes).getRecordAsString(true));
        }
        System.out.println("------ End Records \n");
        System.out.println("------ Hash Indexes");
        for(Integer col : columnIdToHashFileMap.keySet()) {
        	System.out.print("HashColID = " + col );
        	columnIdToHashFileMap.get(col).printDebug();
        }
        System.out.println("----- End Hash Indexes \n");
		System.out.println("-------- END HEAP FILE -----------");
	}
	
	public static void printDebug(String path) {
		HeapFile heapF = new HeapFile(path);
		heapF.printDebug();
	}
	
	public boolean isParameterIndexed(Integer parameter){
		if(this.columnIdToHashFileMap.keySet().contains(parameter))
			return true;
		return false;
	}
}
