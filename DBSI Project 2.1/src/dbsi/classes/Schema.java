package dbsi.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import dbsi.classes.query.Condition;
import dbsi.exception.SchemaException;
import dbsi.exception.TypeException;
import dbsi.helper.ByteHelper;
import dbsi.helper.SchemaHelper;
import dbsi.type.TypeFormat;

public class Schema {

	private String schemaString;
	private long recordLength;
//	private String[] fields;
	private int[] fieldLength;
	private TypeFormat[] fieldTypes;

	/**
	 * creats a new Schema instance with the given Schema string.
	 * if isRidAdded is false, then a column called RID(of type I8) will be added to 
	 * the begning of the schema. 
	 * if isRidAdded is true, then nothing is added.
	 * @param csvSchema
	 * @param isRIdAdded
	 */
	public Schema(String csvSchema, boolean isRIdAdded) {
		if(!isRIdAdded)
			csvSchema = "i8,"+csvSchema;
		this.schemaString = csvSchema;
		String[] csvSchemaSplit = csvSchema.split(",");
//		fields = csvSchemaSplit;
		fieldLength = new int[csvSchemaSplit.length];
		fieldTypes = new TypeFormat[csvSchemaSplit.length];
		try {
			for(int i=0; i<csvSchemaSplit.length; ++i) {
				fieldTypes[i] = SchemaHelper.getTypeForamt(csvSchemaSplit[i]);
				fieldLength[i] = SchemaHelper.getByteSize(csvSchemaSplit[i]);
				this.recordLength += fieldLength[i];
			}
		} catch (TypeException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Converts the given Record string to byte[] so that it can be written to the heap file. 
	 * @param rec
	 * @return
	 * @throws SchemaException
	 */
	public byte[] recordToBytes(String rec) throws SchemaException {
		String[] recFields = rec.split(",");
		if( !(recFields.length == fieldTypes.length) ) {
			throw new SchemaException("Incompatiable Schemas");
		}
		byte[][] fieldByteArys = new byte[recFields.length][];
		for(int i=0; i<recFields.length; ++i){
			if(fieldTypes[i] == null) {
				System.err.println("fieldTypes["+i+"] is NULL - " +
						"fieldTypes.length="+fieldTypes.length+", but recFields.length="+recFields.length);
			}
			fieldByteArys[i] = fieldTypes[i].toRaw(recFields[i], fieldLength[i]);
		}
		return ByteHelper.mergeByteArrays(fieldByteArys);
	}
	
	/**
	 * creates a CursorRecord with fields that belong to the given 
	 * projectionIndexes from the give record byte[]. 
	 * @param record
	 * @param projectionIndexes
	 * @return
	 */
	public CursorRecord recordToCursorRecord(byte[] record, Set<Integer> projectionIndexes) {
		CursorRecord curRec = new CursorRecord();
		int fromIndex = 0;		
		for(int i=0; i<fieldTypes.length; ++i) {
			if(i==0 || projectionIndexes == null || projectionIndexes.size()==0 || projectionIndexes.contains(i)) { 
				curRec.setField(i, fieldTypes[i].toType(record, fromIndex, fieldLength[i]));
				fromIndex += fieldLength[i];
			} else {
				fromIndex += fieldLength[i];
			}
		}
		return curRec;
	}
	
	/**
	 * creates a CursorRecord with fields that belong to the given 
	 * projectionIndexes from the give record byte[], only if the record byte[] 
	 * passes all the given SelectionConditions, otherwise returns null.
	 * @param record
	 * @param selectionMap
	 * @param projectionIndexes
	 * @return
	 */
	public CursorRecord recordToCursorRecord(byte[] record, Map<Integer, ArrayList<Condition>> selectionMap, Set<Integer> projectionIndexes) {
		CursorRecord curRec = new CursorRecord();
		int fromIndex = 0;
		Set<Integer> selectionMapKeys = selectionMap.keySet();
		List<Condition> curConditions = null;
		for(int i=0; i<fieldTypes.length; ++i) {
			if(i==0) {		// always read the recId field.
				curRec.setField(i, fieldTypes[i].toType(record, fromIndex, fieldLength[i]));
				fromIndex += fieldLength[i];
			} else {
				if(selectionMapKeys.contains(i)) {		// current field index is part of selectionIndex
					curConditions = selectionMap.get(i);	// get the conditions
					boolean isPassed = true;
					for(Condition cond : curConditions) {
						int compResult = fieldTypes[i].compare(record, fromIndex, cond.getValue(), fieldLength[i]);
						isPassed = isPassed && cond.getOperator().getCompareResultAllowedForOperator(compResult);
					}
					if(isPassed) {		// if the field passes the selection criteria, process it 
						// read the field is it is a part of projectionIndex
						if(projectionIndexes == null || projectionIndexes.size()==0 || projectionIndexes.contains(i)) { 
							curRec.setField(i, fieldTypes[i].toType(record, fromIndex, fieldLength[i]));
							fromIndex += fieldLength[i];
						} else { 	// skip the field is it is NOT a part of projectionIndex
							fromIndex += fieldLength[i];
						}
					} else {			// if the field fails the selection criteria, skip it
						return null;
					}
				} else {		// current field not part of selectionIndex
					// read the field is it is a part of projectionIndex
					if(projectionIndexes == null || projectionIndexes.size()==0 || projectionIndexes.contains(i)) { 
						curRec.setField(i, fieldTypes[i].toType(record, fromIndex, fieldLength[i]));
						fromIndex += fieldLength[i];
					} else { 	// skip the field is it is NOT a part of projectionIndex
						fromIndex += fieldLength[i];
					}
				}
			}
		}
		return curRec;
	}
	
	/**
	 * returns true if the record passed the selections conditions, 
	 * otherwise returns false
	 * @param record
	 * @param selectionMap
	 * @return
	 */
	public boolean isSelectionPassed(byte[] record, Map<Integer, ArrayList<Condition>> selectionMap) {		
		int fromIndex = 0;
		Set<Integer> selectionMapKeys = selectionMap.keySet();
		List<Condition> curConditions = null;
		for(int i=0; i<fieldTypes.length; ++i) {
			if(i==0) {		// always read the recId field.
				fromIndex += fieldLength[i];
			} else {
				if(selectionMapKeys.contains(i)) {		// current field index is part of selectionIndex
					curConditions = selectionMap.get(i);	// get the conditions
					boolean isPassed = true;
					for(Condition cond : curConditions) {
						int compResult = fieldTypes[i].compare(record, fromIndex, cond.getValue(), fieldLength[i]);
						isPassed = isPassed && cond.getOperator().getCompareResultAllowedForOperator(compResult);
					}
					if(!isPassed) {		// even if any ONE selection condition is NOT passed, return false 
						return false;
					}
				} 
				fromIndex += fieldLength[i];
			}
		}
		return true;	// All selection condition are passed, return true
	}
	
	public String getColumnValue(byte[] record, int colID) throws SchemaException {
		int fromIndex = 0;
		for(int i=0; i<fieldTypes.length; ++i) {
			if(i==colID) {
				return fieldTypes[i].toType(record, fromIndex, fieldLength[i]);
			} else {
				fromIndex += fieldLength[i];
			}
		}
		throw new SchemaException(" ColID("+colID+") not present in the currrent Schema!!!");
	}

	public String getSchemaString() {
		return schemaString;
	}
	public long getRecordLength() {
		return recordLength;
	}
	public int getFieldLength(int colID) {
		return this.fieldLength[colID];
	}
	public TypeFormat getFieldType(int colID) {
		return this.fieldTypes[colID];
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[Schema [schemaString:").append(this.schemaString).
				append("][recordLength:").append(this.recordLength).append("] ]");
		return sb.toString();
	}
	
	@Override
	public boolean equals(Object paramObject) {
		if(this == paramObject) {
			return true;
		}
		if(paramObject instanceof Schema) {
			Schema tempSchema = (Schema) paramObject;
			if(this.schemaString.equals(tempSchema.schemaString) && this.recordLength == tempSchema.recordLength) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
