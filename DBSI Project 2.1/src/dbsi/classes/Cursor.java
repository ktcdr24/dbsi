package dbsi.classes;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import dbsi.exception.CursorException;

public class Cursor {

	String schema = null;
	Long curRecId;
	Set<Long> records = new LinkedHashSet<Long>();
	Set<Integer> projectionIndexes = null;

	/**
	 * add the recID to the cursor.
	 * @param recID
	 */
	public void addRecord(String recID) {
		Long recIDLong = Long.parseLong(recID);
		try {
			if (records.contains(recID))
				throw new CursorException("RecId(" + recID + ") is already present in Cursor.");
			records.add(recIDLong);
		} catch (CursorException e) {
			e.printStackTrace();
		}
	}

	/**
	 * add all the recID's to the Cursor.
	 * @param recIDs
	 */
	public void addAllRecords(List<String> recIDs) {
		Long recIDLong = null;
		for(String recIDStr : recIDs) {
			try {
				recIDLong = Long.parseLong(recIDStr);
				if(records.contains(recIDLong))
					throw new CursorException("RecId(" + recIDStr + ") is already present in Cursor.");
				records.add(recIDLong);
			} catch (CursorException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * add all the recID's to the Cursor.
	 * @param recIDs
	 */
	public void addAllRecords(Set<Long> recIDs) {
		if(recIDs == null || recIDs.size() == 0)
			return;
		this.records.addAll(recIDs);
	}

	public void setSchema(String schema) {
		this.schema = schema;
	}

	public String getSchema() {
		return this.schema;
	}
	
	/**
	 * get the schema of the colums that are projected.
	 * @return
	 */
	public String getProjectedSchema() {
		String[] schemaStringSplit = this.schema.split(",");
		StringBuffer cursorSchemaString = new StringBuffer();
		for(int i=1; i<schemaStringSplit.length; ++i) {
			if(projectionIndexes==null || projectionIndexes.size()==0 || projectionIndexes.contains(i))
				cursorSchemaString.append(schemaStringSplit[i]).append(",");
		}
		cursorSchemaString.setLength(cursorSchemaString.length()-1);
		return cursorSchemaString.toString();
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[Cursor: ");
		for (Long k : records)
			sb.append(k).append(",");
		sb.append("]");
		return sb.toString();
	}

	class CursorIterator implements Iterator<Long> {
		Iterator<Long> recIterator = records.iterator();

		@Override
		public boolean hasNext() {
			return recIterator.hasNext();
		}

		@Override
		public Long next() {
			return recIterator.next();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException(
					"remove() is not implemented for Cursor");
		}

	}

	public CursorIterator iterator() {
		return new CursorIterator();
	}

	public Set<Integer> getProjectionIndexes() {
		return projectionIndexes;
	}
	public void setProjectionIndexes(Set<Integer> projectionIndexes) {
		this.projectionIndexes = projectionIndexes;
	}
	
	public int getNumRecords() {
		return records.size();
	}

}
