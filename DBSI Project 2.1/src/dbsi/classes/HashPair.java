package dbsi.classes;

public class HashPair {

	String value;
	Long recId;
	
	public HashPair() {}
	
	public HashPair(String value, long recId) {
		this.value = value;
		this.recId = recId;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public Long getRecId() {
		return recId;
	}
	public void setRecId(Long recId) {
		this.recId = recId;
	}
	
	public boolean isEmpty() {
		return this.recId == 0;
	}
	
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("[HashPair [value=").append(this.value).
				append("][recId=").append(this.recId).
				append("] ]");
		return sb.toString();
	}
	
	public void printDebug() {
		System.out.print("<" + this.value + ", " + this.recId + ">");
	}
	
}
