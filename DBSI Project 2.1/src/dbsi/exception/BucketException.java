package dbsi.exception;

public class BucketException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2868169580697719603L;

	public BucketException(String msg) {
		super(msg);
	}
	
}
