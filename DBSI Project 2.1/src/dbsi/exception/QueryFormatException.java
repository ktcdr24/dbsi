package dbsi.exception;

public class QueryFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9133369092774056283L;

	public QueryFormatException(String msg) {
		super(msg);
	}
	
}
