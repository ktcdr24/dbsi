package dbsi.exception;

public class CursorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8568692261965627481L;

	public CursorException(String msg) {
		super(msg);
	}
	
}
