package dbsi.exception;

public class OperatorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2538598938862021773L;

	public OperatorException(String msg) {
		super(msg);
	}
	
}
