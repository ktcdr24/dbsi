package dbsi.exception;

public class SchemaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4610367750944496351L;

	public SchemaException(String msg) {
		super(msg);
	}
	
}
