package dbsi.exception;

public class OverflowException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2551121636047039194L;

	public OverflowException(String msg) {
		super(msg);
	}
	
}
