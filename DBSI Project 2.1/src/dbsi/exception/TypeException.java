package dbsi.exception;

public class TypeException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2868169580697719603L;

	public TypeException(String msg) {
		super(msg);
	}
	
}
