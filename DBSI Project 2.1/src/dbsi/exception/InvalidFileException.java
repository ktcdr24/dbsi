package dbsi.exception;

public class InvalidFileException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1785458659893988092L;

	public InvalidFileException(String msg) {
		super(msg);
	}
	
}
