package dbsi.exception;

public class HashException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6355806067838175347L;

	public HashException(String msg) {
		super(msg);
	}
	
}
