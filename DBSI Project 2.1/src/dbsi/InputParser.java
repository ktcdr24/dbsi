package dbsi;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;

import dbsi.classes.HeapFile;
import dbsi.classes.query.Condition;
import dbsi.classes.query.Operator;
import dbsi.classes.query.Query;
import dbsi.classes.query.QueryBuildHash;
import dbsi.classes.query.QueryInsert;
import dbsi.classes.query.QuerySelect;
import dbsi.classes.query.QueryType;
import dbsi.exception.OperatorException;
import dbsi.exception.QueryFormatException;
import dbsi.helper.HeapFileHelper;

public class InputParser {

	private static InputParser inputParser;

	private InputParser() {
	}

	public static InputParser getInstance() {
		if (inputParser == null) {
			inputParser = new InputParser();
		}
		return inputParser;
	}

	public static Boolean isQueryValid(String[] args)
			throws QueryFormatException {
		if(Main.IS_WINDOWS) return true; 
		// Checking if the first arguement or last arguement is an operator
		if (Operator.isStringOperator(args[0])
				|| Operator.isStringOperator(args[args.length - 1]))
			throw new QueryFormatException("Invalid query");
		boolean isSelectionPresent = false;
		boolean isProjectionPresent = false;
		boolean isOperatorPresent = false;
		boolean isHeapFileCreated = false;
		HeapFile heapFile;
		String schema;
		int numberOfFields = 0;
		int count = 0;
		for (int i = 1; i < args.length; i++) {
			if (args[i].equalsIgnoreCase("-i")) {
				if (i == 1 ) {
					if(args.length==2)
						return true;
					else if(args.length>2)
					{
						if(HeapFileHelper.isHeapFilePresent(args[0])) {
							heapFile = new HeapFile(args[0]);
							schema = heapFile.getSchemaAsString();
							numberOfFields = schema.split(",").length - 1;
							for(int j=2;j<args.length;j++)
							{
								if(args[j].startsWith("-b")) {
									try{
										int parameter = Integer.parseInt(args[j].substring(2));
										if (parameter <= numberOfFields)
											continue;
										else
											throw new QueryFormatException(
													"Invalid insert condition, field number exceeds the number of fields"
														+ args[i]);
										} catch (NumberFormatException e) {
											throw new QueryFormatException(
											"Invalid insert Condition" + args[j]);
										}
								} else 
									throw new QueryFormatException("Invalid insert query");
							}
						}
						return true;
					}					
				} else {
					throw new QueryFormatException("Invalid insert query");
				}
			}
			else if (args[i].startsWith("-b")){
				if(i==1)
				{
					if(HeapFileHelper.isHeapFilePresent(args[0]))
						heapFile = new HeapFile(args[0]);
					else
						throw new QueryFormatException("File doesn't exist"); 
					schema = heapFile.getSchemaAsString();
					numberOfFields = schema.split(",").length - 1;
					for(int j=1;j<args.length;j++)
					{
						if(args[j].startsWith("-b"))
						{
							try{
								int parameter = Integer.parseInt(args[j].substring(2));
								if (parameter <= numberOfFields)
									continue;
								else
									throw new QueryFormatException(
											"Invalid selection condition, field number exceeds the number of fields"
												+ args[j]);
								} catch (NumberFormatException e) {
									throw new QueryFormatException(
									"Invalid build index Condition" + args[j]);
								}
						}
						else
							throw new QueryFormatException("Invalid build query");
					}
					return true;
				}else
					throw new QueryFormatException("Invalid build index query");
			}
			else if (args[i].startsWith("-s")) {
				if(!isHeapFileCreated)
				{
					isHeapFileCreated=true;
					heapFile = new HeapFile(args[0]);
					schema = heapFile.getSchemaAsString();
					numberOfFields = schema.split(",").length - 1;
				}
				if (!isSelectionPresent)
					isSelectionPresent = true;
				boolean operatorOnLeft = false;
				boolean operatorOnRight = false;
				if (i - 1 >= 0 && Operator.isStringOperator(args[i - 1]))
					operatorOnLeft = true;
				if (i + 1 <= args.length - 1
						&& Operator.isStringOperator(args[i + 1]))
					operatorOnRight = true;
				if (operatorOnLeft && operatorOnRight)
					throw new QueryFormatException("Invalid select query"
							+ args[i]);
				else if (operatorOnLeft || operatorOnRight) {
					try {
						int parameter = Integer.parseInt(args[i].substring(2));
						if (parameter <= numberOfFields)
							continue;
						else
							throw new QueryFormatException(
									"Invalid selection condition, field number exceeds the number of fields"
											+ args[i]);
						} catch (NumberFormatException e) {
						throw new QueryFormatException(
								"Invalid Selection Condition" + args[i]);
						}
				} else
					throw new QueryFormatException("Invalid select query"
							+ args[i]);
			} else if (Operator.isStringOperator(args[i])) {
				count = 0;
				if (!isOperatorPresent)
					isOperatorPresent = true;
				if (i - 1 >= 0 && Operator.isStringOperator(args[i - 1])
						|| i + 1 <= args.length - 1
						&& Operator.isStringOperator(args[i + 1])) {
					throw new QueryFormatException("Invalid use of conditions"
							+ args[i]);
				}
				if (i > 1 && args[i - 1].startsWith("-s")) {
					continue;
				} else {
					if (args[i].equalsIgnoreCase(">")) {
						if (i == args.length - 2) {
							continue;
						} else if (i == 1 && args.length == 3) {
							continue;
						} else {
							throw new QueryFormatException(
									"Invalid use of condition" + args[i]);
						}
					} else
						throw new QueryFormatException(
								"Invalid use of condition" + args[i]);

				}
			} else if (args[i].startsWith("-p")) {
				if(!isHeapFileCreated)
				{
					isHeapFileCreated=true;
					heapFile = new HeapFile(args[0]);
					schema = heapFile.getSchemaAsString();
					numberOfFields = schema.split(",").length - 1;
				}
				try {
					int parameter = Integer.parseInt(args[i].substring(2));
					if (parameter > numberOfFields)
						throw new QueryFormatException(
								"Invalid projection condition , field number exceeds the number of fields"
										+ args[i]);
				} catch (NumberFormatException e) {
					throw new QueryFormatException(
							"Invalid projection Condition" + args[i]);
				}
				if (!isProjectionPresent)
					isProjectionPresent = true;
				if (i - 1 >= 0 && Operator.isStringOperator(args[i - 1])) {
					throw new QueryFormatException("Invalid use of conditions"
							+ args[i]);
				}
				if (i == args.length - 1)
					continue;
				else if (i + 1 == args.length - 1) {
					if (args[i + 1].startsWith("-p"))
						continue;
					else
						throw new QueryFormatException("Invalid projection"
								+ args[i]);
				} else {
					if (args[i + 1].startsWith("-p")
							|| args[i + 1].startsWith("-s")
							|| args[i + 1].equalsIgnoreCase(">"))
						continue;
					else
						throw new QueryFormatException("Invalid projction"
								+ args[i]);
				}
			} else {
				if ((!isProjectionPresent && !isSelectionPresent && !isOperatorPresent)
						|| (count >= 1))
					throw new QueryFormatException("Invalid query");
				count++;
			}

		}
		if ((!isProjectionPresent && !isSelectionPresent)) {
			if ((args.length == 3 && args[1].equalsIgnoreCase(">"))
					|| args.length == 1)
				return true;
			else
				throw new QueryFormatException(
						"Invalid select query- No selection or Projection conditions mentioned");
		}
		return true;
	}

	public Query parseInput(String[] args) throws QueryFormatException {

		String filePath = "";
		if (args[0] != null) // args[0] is always heapfile path
			filePath = args[0];
		else
			throw new QueryFormatException("Heap File Path is not found");
		if (args.length > 1 && args[1] != null
				&& args[1].equalsIgnoreCase("-i")) { // -i is always used for
														// insert query
			QueryInsert queryInsert = new QueryInsert(filePath,
					QueryType.INSERT);
			queryInsert.setImportFilePath(args[args.length-1]);
			for( int i=2;i<args.length;i++)
			{
				if(args[i].startsWith("-b"))
				{
					queryInsert.getHashColumnIds().add(Integer.parseInt(args[i].substring(2)));
				}
			}
			// if args[2] is "<" then ignore it
	/*		if (args[2].equalsIgnoreCase("<"))
				queryInsert.setImportFilePath(args[3]);
			else
				queryInsert.setImportFilePath(args[2]);*/
			if(Main.VERBOSE) System.out.println("Insert Query -> " + queryInsert);
			return queryInsert; 
		} else if(args.length>1 &&  args[1] != null && args[1].startsWith("-b")){
			QueryBuildHash queryBuildHash = new QueryBuildHash(args[0], QueryType.BUILD_HASH);
			for(int i=1;i<args.length;i++)
			{
				queryBuildHash.getColumnIds().add(Integer.parseInt(args[i].substring(2)));
			}
			return queryBuildHash;
		}
		else { // args[1] is != -i, then it is a select query
			QuerySelect querySelect = new QuerySelect(filePath,
					QueryType.SELECT);
			querySelect.setHeapFilePath(args[0]);
			if (args.length == 1) {
				querySelect.setParametersPresent(false);
			}else 
			{
			querySelect.setParametersPresent(true);
			HeapFile heapFile = new HeapFile(querySelect.getHeapFilePath());
			Map<Integer, ArrayList<Condition>> queryConditions = new LinkedHashMap<Integer, ArrayList<Condition>>();
			Map<Integer, ArrayList<Condition>> equalityConditions = new LinkedHashMap<Integer,ArrayList<Condition>>();
			Integer parameter = 0;
			Condition condition = null;
			ArrayList<Condition> listOfConditions = new ArrayList<Condition>();
			LinkedHashSet<Integer> projections = new LinkedHashSet<Integer>();

			for (int i = 1; i < args.length; i++) {
				condition = new Condition();
				if (args[i].startsWith("-s")) {
					parameter = Integer.parseInt(args[i].substring(2));
					try {
						if(Operator.getOperator(args[i+1])== Operator.EQUALS && heapFile.isParameterIndexed(parameter))
						{
							condition.setOperator(Operator
									.getOperator(args[i + 1]));
							condition.setValue(args[i + 2]);
							if (!equalityConditions.containsKey(parameter)) {
								listOfConditions = new ArrayList<Condition>();
								listOfConditions.add(condition);
								equalityConditions.put(parameter, listOfConditions);
							} else {
								listOfConditions = equalityConditions.get(parameter);
								listOfConditions.add(condition);
							}
						}
						else
						{
							condition.setOperator(Operator
								.getOperator(args[i + 1]));
							condition.setValue(args[i + 2]);
							if (!queryConditions.containsKey(parameter)) {
								listOfConditions = new ArrayList<Condition>();
								listOfConditions.add(condition);
								queryConditions.put(parameter, listOfConditions);
							} else {
								listOfConditions = queryConditions.get(parameter);
								listOfConditions.add(condition);
							}
						}
					} catch (OperatorException e) {
						e.printStackTrace();
					}	
						i += 2;
				} else if (args[i].startsWith("-p")) {
						projections.add(Integer.parseInt(args[i].substring(2)));
				} else if (args[i].startsWith(">")) {
						//querySelect.setExportFilePath(null);
						querySelect.setExportFilePath(args[i + 1]);
				}
			}
				
			//	querySelect.setExportFilePath(null);
				querySelect.setEqualityConditions(equalityConditions);
				querySelect.setProjections(projections);
				querySelect.setQueryConditions(queryConditions);
			}
			if(Main.VERBOSE) System.out.println("Select Query -> " + querySelect);
			return querySelect;
		}
	}

	public Query parseInput_linux(String[] args) throws QueryFormatException {

		String filePath = "";
		if (args[0] != null) // args[0] is always heapfile path
			filePath = args[0];
		else
			throw new QueryFormatException("Heap File Path is not found");
		if (args.length > 1 && args[1] != null
				&& args[1].equalsIgnoreCase("-i")) { // -i is always used for
														// insert query
			QueryInsert queryInsert = new QueryInsert(filePath,
					QueryType.INSERT);
			queryInsert.setImportFilePath(null);
			for( int i=2;i<args.length;i++)
			{
				if(args[i].startsWith("-b"))
				{
					queryInsert.getHashColumnIds().add(Integer.parseInt(args[i].substring(2)));
				}
			}
			// if args[2] is "<" then ignore it
	/*		if (args[2].equalsIgnoreCase("<"))
				queryInsert.setImportFilePath(args[3]);
			else
				queryInsert.setImportFilePath(args[2]);*/
			if(Main.VERBOSE) System.out.println("Insert Query -> " + queryInsert);
			return queryInsert; 
		} else if(args.length>1 &&  args[1] != null && args[1].startsWith("-b")){
			QueryBuildHash queryBuildHash = new QueryBuildHash(args[0], QueryType.BUILD_HASH);
			for(int i=1;i<args.length;i++)
			{
				queryBuildHash.getColumnIds().add(Integer.parseInt(args[i].substring(2)));
			}
			return queryBuildHash;
		}
		else { // args[1] is != -i, then it is a select query
			QuerySelect querySelect = new QuerySelect(filePath,
					QueryType.SELECT);
			querySelect.setHeapFilePath(args[0]);
			if (args.length == 1) {
				querySelect.setParametersPresent(false);
			}else 
			{
			querySelect.setParametersPresent(true);
			HeapFile heapFile = new HeapFile(querySelect.getHeapFilePath());
			Map<Integer, ArrayList<Condition>> queryConditions = new LinkedHashMap<Integer, ArrayList<Condition>>();
			Map<Integer, ArrayList<Condition>> equalityConditions = new LinkedHashMap<Integer,ArrayList<Condition>>();
			Integer parameter = 0;
			Condition condition = null;
			ArrayList<Condition> listOfConditions = new ArrayList<Condition>();
			LinkedHashSet<Integer> projections = new LinkedHashSet<Integer>();

			for (int i = 1; i < args.length; i++) {
				condition = new Condition();
				if (args[i].startsWith("-s")) {
					parameter = Integer.parseInt(args[i].substring(2));
					try {
						if(Operator.getOperator(args[i+1])== Operator.EQUALS && heapFile.isParameterIndexed(parameter))
						{
							condition.setOperator(Operator
									.getOperator(args[i + 1]));
							condition.setValue(args[i + 2]);
							if (!equalityConditions.containsKey(parameter)) {
								listOfConditions = new ArrayList<Condition>();
								listOfConditions.add(condition);
								equalityConditions.put(parameter, listOfConditions);
							} else {
								listOfConditions = equalityConditions.get(parameter);
								listOfConditions.add(condition);
							}
						}
						else
						{
							condition.setOperator(Operator
								.getOperator(args[i + 1]));
							condition.setValue(args[i + 2]);
							if (!queryConditions.containsKey(parameter)) {
								listOfConditions = new ArrayList<Condition>();
								listOfConditions.add(condition);
								queryConditions.put(parameter, listOfConditions);
							} else {
								listOfConditions = queryConditions.get(parameter);
								listOfConditions.add(condition);
							}
						}
					} catch (OperatorException e) {
						e.printStackTrace();
					}	
						i += 2;
				} else if (args[i].startsWith("-p")) {
						projections.add(Integer.parseInt(args[i].substring(2)));
				} /*else if (args[i].startsWith(">")) {
						querySelect.setExportFilePath(null);
						//querySelect.setExportFilePath(args[i + 1]);
				}*/
			}
				
				querySelect.setExportFilePath(null);
				querySelect.setEqualityConditions(equalityConditions);
				querySelect.setProjections(projections);
				querySelect.setQueryConditions(queryConditions);
			}
			if(Main.VERBOSE) System.out.println("Select Query -> " + querySelect);
			return querySelect;
		}
	}
}
