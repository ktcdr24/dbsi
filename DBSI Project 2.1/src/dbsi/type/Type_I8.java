package dbsi.type;

import dbsi.helper.ByteHelper;

public class Type_I8 implements TypeFormat {

	@Override
	public byte[] toRaw(String data, int length) {
		long x = Long.parseLong(data);
		return ByteHelper.i8ToRaw(x);
	}
	
	@Override
	public String toType(byte[] b) {
		return ByteHelper.rawToI8(b, 0) + "";
	}

	@Override
	public String toType(byte[] record, int offset, int length) {
		return ByteHelper.rawToI8(record, offset) + "";
	}
	
	@Override
	public int compare(byte[] record1, int offset1, byte[] record2, int offset2, int length) {
		long x = ByteHelper.rawToI8(record1, offset1);
		long y = ByteHelper.rawToI8(record2, offset2);
		return x>y ? 1 : (x<y ? -1 : 0);
	}

	@Override
	public int compare(byte[] record, int offset, String data, int length) {
		long x = ByteHelper.rawToI8(record, offset);
		long y = Long.parseLong(data);
		return x>y ? 1 : (x<y ? -1 : 0);
	}
	
	@Override
	public Long getHashForValue(String value) {
		try{
			Long l = Long.parseLong(value);
			l = (l.longValue() < 0) ? l.longValue()*-1 : l.longValue();
			return l;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}
