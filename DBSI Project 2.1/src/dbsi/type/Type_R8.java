package dbsi.type;

import dbsi.helper.ByteHelper;

public class Type_R8 implements TypeFormat {

	@Override
	public byte[] toRaw(String data, int length) {
		double d = Double.parseDouble(data);
		return ByteHelper.r8ToRaw(d);
	}

	@Override
	public String toType(byte[] record, int offset, int length) {
		return ByteHelper.rawToR8(record, offset) + "";
	}

	@Override
	public String toType(byte[] b) {
		return ByteHelper.rawToR8(b, 0) + "";
	}

	@Override
	public int compare(byte[] record1, int offset1, byte[] record2, int offset2, int length) {
		double x = ByteHelper.rawToR8(record1, offset1);
		double y = ByteHelper.rawToR8(record2, offset2);
		double diff = x - y;
		if(EPSILON_DOUBLE_PRECISION*-1 < diff && diff < EPSILON_DOUBLE_PRECISION)
			return 0;
		else if (diff > EPSILON_DOUBLE_PRECISION)
			return 1;
		else 
			return -1;			
	}

	@Override
	public int compare(byte[] record, int offset, String data, int length) {
		double x = ByteHelper.rawToR8(record, offset);
		double y = Double.parseDouble(data);
		double diff = x - y;
		if(EPSILON_DOUBLE_PRECISION*-1 < diff && diff < EPSILON_DOUBLE_PRECISION)
			return 0;
		else if (diff > EPSILON_DOUBLE_PRECISION)
			return 1;
		else 
			return -1;	
	}
	
	@Override
	public Long getHashForValue(String value) {
		try{
//			Long l = Double.doubleToLongBits(Double.parseDouble(value));
//			l = (l.longValue() < 0) ? l.longValue()*-1 : l.longValue();
			int dot = value.indexOf(".");
			String intPart = value.substring(0, dot);
			String floatPart = value.substring(dot + 1); 
			if(floatPart.length() > 4) 
				floatPart = floatPart.substring(0, 4);
			Long l = Long.parseLong(intPart + floatPart);
			l = (l.longValue() < 0) ? l.longValue()*-1 : l.longValue();
			return l;
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}
