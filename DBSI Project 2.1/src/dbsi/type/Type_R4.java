package dbsi.type;

import dbsi.helper.ByteHelper;

public class Type_R4 implements TypeFormat {
	
	

	@Override
	public byte[] toRaw(String data, int length) {
		double d = Double.parseDouble(data);
		return ByteHelper.r4ToRaw(d);
	}

	@Override
	public String toType(byte[] record, int offset, int length) {
		return ByteHelper.rawToR4(record, offset) + "";
	}

	@Override
	public String toType(byte[] b) {
		return ByteHelper.rawToR4(b, 0) + "";
	}

	@Override
	public int compare(byte[] record1, int offset1, byte[] record2, int offset2, int length) {
		double x = ByteHelper.rawToR4(record1, offset1);
		double y = ByteHelper.rawToR4(record2, offset2);
		double diff = x - y;
		if(EPSILON_FLOAT_PRECISION*-1 < diff && diff < EPSILON_FLOAT_PRECISION)
			return 0;
		else if (diff > EPSILON_FLOAT_PRECISION)
			return 1;
		else 
			return -1;			
	}

	@Override
	public int compare(byte[] record, int offset, String data, int length) {
		float x = (float)ByteHelper.rawToR4(record, offset);
		float y = Float.parseFloat(data);
		float diff = x - y;
		if(EPSILON_FLOAT_PRECISION*-1 < diff && diff < EPSILON_FLOAT_PRECISION)
			return 0;
		else if (diff > EPSILON_FLOAT_PRECISION)
			return 1;
		else 
			return -1;	
	}
	
	@Override
	public Long getHashForValue(String value) {
		try{
//			Integer i = Float.floatToIntBits(Float.parseFloat(value));
//			i = (i.intValue() < 0) ? i.intValue()*-1 : i.intValue();
			int dot = value.indexOf(".");
			String intPart = value.substring(0, dot);
			String floatPart = value.substring(dot + 1); 
			if(floatPart.length() > 4) 
				floatPart = floatPart.substring(0, 4);
			Integer i = Integer.parseInt(intPart + floatPart);
			i = (i.intValue() < 0) ? i.intValue()*-1 : i.intValue();
			return new Long(i);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}
