package dbsi.type;

import dbsi.helper.ByteHelper;

public class Type_I4 implements TypeFormat {

	@Override
	public byte[] toRaw(String data, int length) {
		int x = Integer.parseInt(data);
		return ByteHelper.i4ToRaw(x);
	}
	
	@Override
	public String toType(byte[] b) {
		return ByteHelper.rawToI4(b, 0) + "";
	}

	@Override
	public String toType(byte[] record, int offset, int length) {
		return ByteHelper.rawToI4(record, offset) + "";
	}
	
	@Override
	public int compare(byte[] record1, int offset1, byte[] record2, int offset2, int length) {
		int x = ByteHelper.rawToI4(record1, offset1);
		int y = ByteHelper.rawToI4(record2, offset2);
		return x>y ? 1 : (x<y ? -1 : 0);
	}

	@Override
	public int compare(byte[] record, int offset, String data, int length) {
		int x = ByteHelper.rawToI4(record, offset);
		int y = Integer.parseInt(data);
		return x>y ? 1 : (x<y ? -1 : 0);
	}
	
	@Override
	public Long getHashForValue(String value) {
		try{
			Integer i = Integer.parseInt(value);
			i = (i.intValue() < 0) ? i.intValue()*-1 : i.intValue();
			return new Long(i);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}
