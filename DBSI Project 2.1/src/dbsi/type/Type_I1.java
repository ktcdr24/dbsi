package dbsi.type;

import dbsi.helper.ByteHelper;

public class Type_I1 implements TypeFormat {

	@Override
	public byte[] toRaw(String data, int length) {
		int x = Integer.parseInt(data);
		return ByteHelper.i1ToRaw(x);
	}
	
	@Override
	public String toType(byte[] b) {
		return ByteHelper.rawToI1(b, 0) + "";
	}

	@Override
	public String toType(byte[] record, int offset, int length) {
		return ByteHelper.rawToI1(record, offset) + "";
	}
	
	@Override
	public int compare(byte[] record1, int offset1, byte[] record2, int offset2, int length) {
		int x = ByteHelper.rawToI1(record1, offset1);
		int y = ByteHelper.rawToI1(record2, offset2);
		return x>y ? 1 : (x<y ? -1 : 0);
	}

	@Override
	public int compare(byte[] record, int offset, String data, int length) {
		int x = ByteHelper.rawToI1(record, offset);
		int y = Integer.parseInt(data);
		return x>y ? 1 : (x<y ? -1 : 0);
	}
	
	@Override
	public Long getHashForValue(String value) {
		try{
			Short s = Short.parseShort(value);
			s = (short) (s.shortValue()<0 ? (s.shortValue()*(short)-1) : s.shortValue());
			return new Long(s.shortValue());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return null;
	}

}
