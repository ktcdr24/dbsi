package dbsi.type;

import dbsi.helper.ByteHelper;

public class Type_Cx implements TypeFormat {

	@Override
	public byte[] toRaw(String data, int length) {
		return ByteHelper.cxToRaw( (String.format("%1$"+length+"s", data)).toCharArray() );
	}

	@Override
	public String toType(byte[] record, int offset, int length) {
		byte[] cxByte = new byte[length];
		for(int i=0; i<length; ++i)
			cxByte[i] = record[offset + i];
		return toType(cxByte);
	}
	
	@Override
	public String toType(byte[] b) {
		return new String(ByteHelper.rawToCx(b));
	}

	@Override
	public int compare(byte[] record1, int offset1, byte[] record2, int offset2, int length) {
		
		return 0;
	}
	
	@Override
	public int compare(byte[] record, int offset, String data, int length) {
		String x = toType(record, offset, length);
		int comp = x.trim().compareTo(data.trim());
		return comp>0 ? 1 : (comp<0 ? -1 : 0);
	}
	
	@Override
	public Long getHashForValue(String value) {
		char[] valAry = value.trim().toUpperCase().toCharArray();
		Long valAryTotal = new Long(0);
		for(int i=0; i<valAry.length; ++i) {
			valAryTotal += (long)valAry[i];
		}
		return valAryTotal;
	}

}
