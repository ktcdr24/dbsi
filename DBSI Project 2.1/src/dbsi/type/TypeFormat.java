package dbsi.type;

public interface TypeFormat {
	
	public static double EPSILON_DOUBLE_PRECISION = 0.00001;
	public static double EPSILON_FLOAT_PRECISION = 0.0001;
	
	public byte[] toRaw(String data, int length);
	public String toType(byte[] record, int offset, int length);
	public String toType(byte[] b);
//	public int compare(
//			byte[] record1, int offset1, 
//			byte[] record2,	int offset2);
//	public int compare(
//			byte[] record, int offset, 
//			String data);
	public int compare(
			byte[] record1, int offset1, 
			byte[] record2,	int offset2,
			int length);
	public int compare(
			byte[] record, int offset, 
			String data,
			int length);	
	public Long getHashForValue(String value);
	
}
