package dbsi;

import dbsi.classes.query.Query;
import dbsi.exception.QueryFormatException;

public class DataBase {
	
	QueryExecutionEngine engine;
	
	public DataBase() {
		engine = new QueryExecutionEngine();
	} 
	
	public void executeParams(String[] args) {
		try {
			if(Main.VERBOSE) System.out.println("DataBase initialized.");
			try{
				InputParser.isQueryValid(args);
			} catch(QueryFormatException exe) {
				System.err.println("Input arguments are invalid. Reason: " + exe.getMessage());
				System.err.println("Database is now Terminating!!!!");
				System.exit(0);
			}
			if(Main.VERBOSE) System.out.println("Input arguments are valid.");
			Query query;
			if(Main.IS_WINDOWS)
				 query = InputParser.getInstance().parseInput(args);
			else
				 query = InputParser.getInstance().parseInput_linux(args);
			if(Main.VERBOSE) System.out.println("Executing Query...");
			engine.executeQuery(query);
			if(Main.VERBOSE) System.out.println("Database closed.");
		} catch (QueryFormatException e) {
			e.printStackTrace();
		}
	}
	
}
