package dbsi.helper;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import dbsi.classes.HashFile;
import dbsi.exception.InvalidFileException;
import dbsi.type.Bucket;
import dbsi.type.TypeFormat;

public class HashFileHelper {
	
	public static File getHashFile(String path) throws InvalidFileException {
		File hashFile = new File(path);
		if(!hashFile.exists()) {
			throw new InvalidFileException("Attempting to get HashFile(" + path + ") which is NOT existing!!!");
		}
		return hashFile;
	}
	
	public static File createNewHashFile(String path, int colLength, TypeFormat colFormat) throws InvalidFileException, IOException {
		File hashFile = new File(path);
		if(hashFile.exists()) {
			throw new InvalidFileException("Attempting to create an HashFile(" + path + ") which is already existing!!!");
		}
		boolean isFileCreated = hashFile.createNewFile();
		if(!isFileCreated)
			throw new InvalidFileException("Could not create the following HashFile: " + path);
		RandomAccessFile raf = new RandomAccessFile(hashFile, "rw");
		try{
			raf.write(makeHeaderForHashFile(colLength));
			raf.write(makeInitialBuckets(colLength, colFormat));
		} finally {
			raf.close();
		}
		return hashFile;
	}
	
	private static byte[] makeHeaderForHashFile(int colLength) {
		byte[][] headerAry = new byte[3][];
		headerAry[0] = ByteHelper.i8ToRaw(1); // nextPointer
		headerAry[1] = ByteHelper.i8ToRaw(HashFile.INITIAL_SIZE.longValue()); // 's' value
		headerAry[2] = ByteHelper.i4ToRaw(colLength); // length of the column on which thae HASH index is built
		byte[] header = ByteHelper.mergeByteArrays(headerAry);				
		return header;
	}
	
	private static byte[] makeInitialBuckets(int colLength, TypeFormat colFormat) {
		Bucket bucket = new Bucket(colLength, colFormat);
		byte[][] initialBucketsByteAry = new byte[HashFile.INITIAL_SIZE][]; 
		for(int i=0; i<initialBucketsByteAry.length; ++i) {
			initialBucketsByteAry[i] = bucket.getBytes();
		}
		return ByteHelper.mergeByteArrays(initialBucketsByteAry);
	}
	
	public static Long getIndexForHash(Long k, Long nextIndex, Long s) {
		long hk = 1 + (k.longValue() % s.longValue());
		if(hk < nextIndex.longValue()) 
			return (k.longValue() % (2*s.longValue()) + 1);
		else 
			return hk;
	}
	
}
