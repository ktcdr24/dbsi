package dbsi.helper;

public class ByteHelper {
	
	/*------------------------------------------------------*/
	
	public static byte[] i1ToRaw(int data) {
		byte[] b = new byte[]{
				(byte)((data >> 0) & 0xff)
		};		
		return b;
	}
	
	public static int rawToI1(byte[] record, int offset) {
		byte[] intByte = new byte[] {
				record[offset]
		};
		return (int)(
				(0xff & intByte[0])
				);
	}
	
	public static byte[] i1ToRaw(byte[] record, int offset, int data) {
		byte[] b = i1ToRaw(data);
		for(int i=0; i<1; ++i)
			record[offset*1 + i] = b[i];
		return record;
	}
	
	/*------------------------------------------------------*/
	
	public static byte[] i2ToRaw(int data) {
		byte[] b = new byte[]{
				(byte)((data >> 8) & 0xff),
				(byte)((data >> 0) & 0xff)
		};		
		return b;
	}
	
	public static int rawToI2(byte[] record, int offset) {
		byte[] intByte = new byte[] {
				record[offset],
				record[offset+1]
		};
		return (int)(
				(0xff & intByte[0]) << 8 | 
				(0xff & intByte[1]) << 0
				);
	}
	
	public static byte[] i2ToRaw(byte[] record, int offset, int data) {
		byte[] b = i2ToRaw(data);
		for(int i=0; i<2; ++i)
			record[offset*2 + i] = b[i];
		return record;
	}
	
	/*------------------------------------------------------*/
	
	public static byte[] i4ToRaw(int data) {
		byte[] b = new byte[]{
				(byte)((data >> 24) & 0xff),
				(byte)((data >> 16) & 0xff),
				(byte)((data >> 8) & 0xff),
				(byte)((data >> 0) & 0xff)
		};		
		return b;
	}
	
	public static int rawToI4(byte[] record, int offset) {
		byte[] intByte = new byte[] {
				record[offset],
				record[offset+1],
				record[offset+2],
				record[offset+3]
		};
		return (int)(
				(0xff & intByte[0]) << 24 | 
				(0xff & intByte[1]) << 16 |
				(0xff & intByte[2]) << 8 |
				(0xff & intByte[3]) << 0
				);
	}
	
	public static byte[] i4ToRaw(byte[] record, int offset, int data) {
		byte[] b = i4ToRaw(data);
		for(int i=0; i<4; ++i)
			record[offset*4 + i] = b[i];
		return record;
	}
	
	/*------------------------------------------------------*/

	public static byte[] i8ToRaw(long data) {
		byte[] b = new byte[]{
				(byte)((data >> 56) & 0xff),
				(byte)((data >> 48) & 0xff),
				(byte)((data >> 40) & 0xff),
				(byte)((data >> 32) & 0xff),
				(byte)((data >> 24) & 0xff),
				(byte)((data >> 16) & 0xff),
				(byte)((data >> 8) & 0xff),
				(byte)((data >> 0) & 0xff)
		};		
		return b;
	}
	
	public static long rawToI8(byte[] record, int offset) {
		byte[] intByte = new byte[] {
				record[offset],
				record[offset+1],
				record[offset+2],
				record[offset+3],
				record[offset+4],
				record[offset+5],
				record[offset+6],
				record[offset+7]
		};
		return (long)(
				(long)(0xff & intByte[0]) << 56 | 
				(long)(0xff & intByte[1]) << 48 |
				(long)(0xff & intByte[2]) << 40 |
				(long)(0xff & intByte[3]) << 32 |
				(long)(0xff & intByte[4]) << 24 |
				(long)(0xff & intByte[5]) << 16 |
				(long)(0xff & intByte[6]) << 8 |
				(long)(0xff & intByte[7]) << 0
				);
	}
	
	public static byte[] i8ToRaw(byte[] record, int offset, long data) {
		byte[] b = i8ToRaw(data);
		for(int i=0; i<8; ++i)
			record[offset*8 + i] = b[i];
		return record;
	}
	
	/*------------------------------------------------------*/
	
	public static byte[] r4ToRaw(double data) {
		byte b[] = i8ToRaw(Double.doubleToRawLongBits(data));
		if(b.length < 4) {
			return b;
		}
		return new byte[]{
				b[0],
				b[1],
				b[2],
				b[3],
		};
	}
	
	public static double rawToR4(byte[] record, int offset) {
		byte emptyByte = 0x0;
		byte[] doubleByte = new byte[] {
				record[offset],
				record[offset+1],
				record[offset+2],
				record[offset+3],
				emptyByte,
				emptyByte,
				emptyByte,
				emptyByte
		};
		return Double.longBitsToDouble(rawToI8(doubleByte, 0));
	}
	
	/*------------------------------------------------------*/
	
	public static byte[] r8ToRaw(double data) {
		return i8ToRaw(Double.doubleToRawLongBits(data));
	}
	
	public static double rawToR8(byte[] record, int offset) {
		byte[] doubleByte = new byte[] {
				record[offset],
				record[offset+1],
				record[offset+2],
				record[offset+3],
				record[offset+4],
				record[offset+5],
				record[offset+6],
				record[offset+7]
		};
		return Double.longBitsToDouble(rawToI8(doubleByte, 0));
	}
	
	/*------------------------------------------------------*/
	// 1 Char is 2 bytes
//	public static byte[] cxToRaw(char data) {
//	    return new byte[] {
//	        (byte)((data >> 8) & 0xff),
//	        (byte)((data >> 0) & 0xff)
//	    };
//	}
//
//	public static byte[] cxToRaw(char[] data) {
//	    if (data == null) 
//	    	return null;
//	    byte[] byts = new byte[data.length * 2];
//	    for (int i = 0; i < data.length; i++)
//	        System.arraycopy(cxToRaw(data[i]), 0, byts, i * 2, 2);
//	    return byts;
//	}
//	
//	public static char rawToChar(byte[] data) {
//	    if (data == null || data.length != 2) 
//	    	return 0x0;
//	    return (char)(
//	            (0xff & data[0]) << 8   |
//	            (0xff & data[1]) << 0
//	            );
//	}
//
//	public static char[] rawToCx(byte[] data) {
//	    if (data == null || data.length % 2 != 0) 
//	    	return null;
//	    char[] chrs = new char[data.length / 2];
//	    for (int i = 0; i < chrs.length; i++) {
//	        chrs[i] = rawToChar( new byte[] {
//	            data[(i*2)],
//	            data[(i*2)+1],
//	        } );
//	    }
//	    return chrs;
//	}
	
	/*------------------------------------------------------*/
	// 1 Char is 1 byte
	
	public static byte[] cxToRaw(char data) {
	    return new byte[] {
	        (byte)((data >> 0) & 0xff)
	    };
	}
	
	public static byte[] cxToRaw(char[] data) {
		byte[] byts = new byte[data.length];
	    for (int i = 0; i < data.length; i++)
	        System.arraycopy(cxToRaw(data[i]), 0, byts, i, 1);
	    return byts;
	}
	
	public static char rawToChar(byte[] data) {
	    if (data == null || data.length != 1) 
	    	return 0x0;
	    return (char)(
	            (0xff & data[0])
	            );
	}
	
	public static char[] rawToCx(byte[] data) {
		if (data == null || data.length == 0) 
	    	return null;
	    char[] chrs = new char[data.length];
	    for (int i = 0; i < chrs.length; i++) {
	        chrs[i] = rawToChar( new byte[] {
	            data[(i)]
	        } );
	    }
	    return chrs;
	}
	
	public static String rawToCx(byte[] record, int offset, int xValue) {
		byte[] b = new byte[xValue];
		for(int i=0; i<xValue; ++i) {
			b[i] = record[offset + i];
		}
		return new String(b);
	}
	
	/*------------------------------------------------------*/
	
	public static byte[] readBytes(byte[] record, int offset, int length) {
		byte[] toRet = new byte[length];
		for(int i=0; i<length; ++i) {
			toRet[i] = record[offset + i];
		}
		return toRet;
	}
	
	public static byte[] mergeByteArrays(byte[][] ary) {
		int numByteArys = ary.length;
		int targetAryLen = 0;
		for(int i=0; i<numByteArys; ++i) {
			targetAryLen += ary[i].length;
		}
		byte[] tragetAry = new byte[targetAryLen];
		int curAryLen = 0;
		int fromIndex = 0;
		for(int i=0; i<numByteArys; ++i) {
			curAryLen = ary[i].length;
			System.arraycopy(ary[i], 0, tragetAry, fromIndex, curAryLen);
			fromIndex += curAryLen;
		}
		return tragetAry;
	}
	
	public static void copyByteAry(byte[] fromSrc, byte[] toDest, int fromIndex) {
		System.arraycopy(fromSrc, fromIndex, toDest, 0, fromSrc.length);
	}
}
