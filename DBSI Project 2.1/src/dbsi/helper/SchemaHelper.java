package dbsi.helper;

import java.util.HashMap;

import dbsi.exception.TypeException;
import dbsi.type.TypeFormat;
import dbsi.type.Type_Cx;
import dbsi.type.Type_I1;
import dbsi.type.Type_I2;
import dbsi.type.Type_I4;
import dbsi.type.Type_I8;
import dbsi.type.Type_R4;
import dbsi.type.Type_R8;

public class SchemaHelper {
	
	private static HashMap<String, Integer> typeSizes = new HashMap<String, Integer>();
	private static final HashMap<String, TypeFormat> types;
	
	static{
		typeSizes.put("i1", 1);
		typeSizes.put("i2", 2);
		typeSizes.put("i4", 4);
		typeSizes.put("i8", 8);
		typeSizes.put("r4", 4);
		typeSizes.put("r8", 8);
		for(int i=1; i<=100; ++i)
			typeSizes.put("c"+i, i);
		types = new HashMap<String, TypeFormat>();
		types.put("i1", new Type_I1());
		types.put("i2", new Type_I2());
		types.put("i4", new Type_I4());
		types.put("i8", new Type_I8());
		types.put("r4", new Type_R4());
		types.put("r8", new Type_R8());
		types.put("cx", new Type_Cx());
	}
	
	/**
	 * returns the number of bytes for the given type
	 * @param type
	 * @return
	 * @throws TypeException - when type is not spported
	 */
	public static int getByteSize(String type) throws TypeException {
		if(typeSizes.containsKey(type))
			return typeSizes.get(type);
		throw new TypeException("Unsupported Type: " + type);
	}
	

	public static TypeFormat getTypeForamt(String type) throws TypeException {
		if(type.startsWith("c"))
			return types.get("cx");
		if(types.containsKey(type))
			return types.get(type);
		throw new TypeException("Unsupported Type: " + type);
	}
	
}
