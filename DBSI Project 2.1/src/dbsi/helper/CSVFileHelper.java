package dbsi.helper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dbsi.Main;
import dbsi.classes.Cursor;
import dbsi.classes.HeapFile;
import dbsi.exception.InvalidFileException;

public class CSVFileHelper {

	
	/**
	 * Reads the input CSV file and return the schema. 
	 * @param path
	 * @return
	 * @throws InvalidFileException - if file is not of type csv
	 * @throws IOException - if file is not found or exception while reading the file 
	 */
	public static String getSchemaFromFile(String path)
			throws InvalidFileException, IOException {
		String schema = null;
		if(path == null) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new InputStreamReader(System.in));
				schema = br.readLine();
			} finally {
				if(br != null) br.close();
			}
		} else {
			int dotIndex = path.lastIndexOf(".");
			String ext = path.substring(dotIndex + 1);
			if (!ext.equals("csv")) {
				throw new InvalidFileException(
						"Import file should have extencion .csv");
			}
			BufferedReader br = null;
			try {
				File csvFile = new File(path);
				br = new BufferedReader(new InputStreamReader(
						new FileInputStream(csvFile)));
				schema = br.readLine();
			} finally {
				if(br != null) br.close();
			}
		}
		return schema;
	}
	
	/**
	 * Getting all records from the given file. 
	 * @param path
	 * @return
	 * @throws InvalidFileException - if this is not of type ".csv"
	 * @throws IOException
	 */
	public static List<String> getAllRecords(String path, Integer start, Integer limit) throws InvalidFileException, IOException {		
		List<String> records = new ArrayList<String>();
		if(path == null) {
			System.exit(0); // shouldnt reach here
		} else {
			int dotIndex = path.lastIndexOf(".");
			String ext = path.substring(dotIndex + 1);
			if (!ext.equals("csv")) {
				throw new InvalidFileException(
						"Import file should have extencion .csv");
			}
			BufferedReader br = null;
			int lineNumber = 1;
			try {
				File csvFile = new File(path);
				br = new BufferedReader(new InputStreamReader(
						new FileInputStream(csvFile)));
				br.readLine(); 	// skip reading schema
				String rec;
				if(Main.DEBUG) System.out.print("<< skipping lineNumber(s)=");
				while(lineNumber < start.intValue() && (rec = br.readLine()) != null) {											
					if(Main.DEBUG) System.out.print(lineNumber+",");
					lineNumber++;
				}
				if(Main.DEBUG) System.out.println();
				if(Main.DEBUG) System.out.print("<< reading lineNumber(s)=");
				while(((lineNumber - start.intValue()) < limit.intValue()) && (rec = br.readLine()) != null) {
					records.add(rec);
					if(Main.DEBUG) System.out.print(lineNumber+",");
					lineNumber++;
				}
				if(Main.DEBUG) System.out.println();
			} finally {
				if(br != null) br.close();
			}
		}
		return records;
	}
	
	public static File createCSVFile(String path) throws InvalidFileException, IOException {
		System.out.println("creating CSV File - " + path);
		File csvFile = new File(path);
		if(csvFile.exists()) {
			csvFile.delete();
			csvFile=new File(path);
		}
		boolean isFileCreated = csvFile.createNewFile();
		if(isFileCreated) {
			
			return csvFile;
		}
		else 
			throw new InvalidFileException("Could not create the following HeapFile: " + path);
	}
	
	public static File addRecords(File file, HeapFile heapFile, Cursor cursor)
	{
		BufferedWriter bufferedWriter = null;
		try {
			// Construct the BufferedWriter object
			bufferedWriter = new BufferedWriter(new FileWriter(file));
			bufferedWriter.write(cursor.getProjectedSchema() + "\n");
			Iterator<Long> iter = cursor.iterator();
			Long recID = null;
			Set<Integer> projectionIndexes = cursor.getProjectionIndexes();
			while (iter.hasNext()) {
				recID = iter.next();
            	String s = heapFile.getCursorRecord(recID, projectionIndexes).getRecordAsString(false);
				bufferedWriter.write(s + "\n");
			}
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			// Close the BufferedWriter
			try {
				if (bufferedWriter != null) {
					bufferedWriter.flush();
					bufferedWriter.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}	
		return file;
	}
}
