package dbsi.helper;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

import dbsi.Main;
import dbsi.classes.Schema;
import dbsi.exception.InvalidFileException;

public class HeapFileHelper {

	/**
	 * Returns the file instance of the file in the given path
	 * @param path
	 * @return
	 * @throws InvalidFileException - if file does not exist.
	 */
	public static File getHeapFile(String path) throws InvalidFileException {
		File heapFile = new File(path);
		if(!heapFile.exists())
			throw new InvalidFileException("Heap file not found at " + path);
		return heapFile;
	}
	
	/**
	 * return true if HeapFile is present in the given path,
	 * false otherwise. 
	 * @param path
	 * @return
	 */
	public static Boolean isHeapFilePresent(String path) {
		return new File(path).exists();
	}
	
	public static String getNewHeapFileName(String heapFileLocation) {
		StringBuffer sb = new StringBuffer();
		int i=1;
		sb.append(heapFileLocation).append("_").append(1);
		while(true) {
			if(!isHeapFilePresent(sb.toString()))
				return sb.toString();
			else {
				i++;
				if(i>10){
					sb.setLength(sb.length() - 2);				
					sb.append(i);
				} else {
					sb.setLength(sb.length() - 1);				
					sb.append(i);
				}
			}
		}
	}
	
	/**
	 * Create a new Heap File
	 * @param path
	 * @return
	 * @throws InvalidFileException
	 * @throws IOException
	 */
	public static File createHeapFile(String path, Schema schema) throws InvalidFileException, IOException {
		File heapFile = new File(path);
		if(heapFile.exists()) {
			throw new InvalidFileException("Attempting to create an HeapFile(" + path + ") which is already existing!!!");
		}
		boolean isFileCreated = heapFile.createNewFile();
		if(isFileCreated) {
			byte[] header = makeHeaderForNewHeapFile(schema);
			RandomAccessFile raf = new RandomAccessFile(heapFile, "rw"); 
			try{
				raf.write(header);
			} finally {
				raf.close();
			}
			if(Main.DEBUG) System.out.println("- HeapFile Created - " + heapFile);
			return heapFile;
		}
		else 
			throw new InvalidFileException("Could not create the following HeapFile: " + path);
	}
	
	/**
	 * 0-i8-HeaderSize; 1-i8-NumRecords; 2-i8-RecLength; 3-i4-SchemaLength; 4-String-Schema;
	 * 5-i2-NumHashColumns; 6-List<i2>-hashColumnID's
	 * @param schema
	 * @return
	 */
	public static byte[] makeHeaderForNewHeapFile(Schema schema) {
		byte[][] headerByteArys = new byte[7][];
		headerByteArys[0] = ByteHelper.i8ToRaw(0); 							// header length
		headerByteArys[1] = ByteHelper.i8ToRaw(0);							// num records
		headerByteArys[2] = ByteHelper.i8ToRaw(schema.getRecordLength()); 	// rec length
		// headerByteArys[3] is added after headerByteArys[4] 
		headerByteArys[4] = ByteHelper.cxToRaw(schema.getSchemaString().toCharArray()); 	// schema string
		headerByteArys[3] = ByteHelper.i4ToRaw(headerByteArys[4].length);	// schema string length
				
		headerByteArys[5] = ByteHelper.i2ToRaw(0);							// Num HashCols
		headerByteArys[6] = new byte[schema.getSchemaString().split(",").length * 2];	// allocate space for HashColIds
		
		byte header[] = ByteHelper.mergeByteArrays(headerByteArys);
		long headerSize = (long)header.length;
		ByteHelper.i8ToRaw(header, 0, headerSize);
		return header;
	}
	
}
