package dbsi.helper;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class InputArgsGen {

	private static int i = 1;
	
	public static void main(String[] args) {
		String csvFilePath = "files/zz.csv";
		String schema = "i4,r8,r4,c10,c10,i2";
//		int[] schemaLength = new int[]{6, 9, 8, 4};
		
		Random r = new Random(System.currentTimeMillis());
		
		File csvFile = new File(csvFilePath);
		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(csvFile));
			bw.write(schema + "\n");
			for(int i=0; i<100; ++i) {
				for(int j=0; j<9; ++j) {
					String s = "F" + i + "," + "L" + j;
					write(bw, s, r);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if(bw != null)
				try {
					bw.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}
	}
	
	private static void write(BufferedWriter bw , String s, Random r) {
		try {
			bw.write(i + "," + 
					r.nextDouble() + "," +
					r.nextFloat() + "," +
					s + "," +
					r.nextInt(100) + "\n");
			i++;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
