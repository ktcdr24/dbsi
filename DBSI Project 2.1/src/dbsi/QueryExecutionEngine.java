package dbsi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dbsi.classes.Cursor;
import dbsi.classes.HeapFile;
import dbsi.classes.query.Query;
import dbsi.classes.query.QueryBuildHash;
import dbsi.classes.query.QueryInsert;
import dbsi.classes.query.QuerySelect;
import dbsi.classes.query.QueryType;
import dbsi.exception.HashException;
import dbsi.exception.InvalidFileException;
import dbsi.exception.SchemaException;
import dbsi.helper.CSVFileHelper;

public class QueryExecutionEngine {
	
	public void executeQuery(Query query) {
		if(query.getQueryType().equals(QueryType.INSERT)) {
			executeInsertQuery((QueryInsert)query);
		} else if (query.getQueryType().equals(QueryType.SELECT)) {
			executeSelectQuery((QuerySelect)query);
		} else if (query.getQueryType().equals(QueryType.BUILD_HASH)) {
			executeBuildHashQuery((QueryBuildHash)query);
		}
	}
	
	private void executeInsertQuery(QueryInsert query) {
		try {
			if(query.getImportFilePath() == null) {
				try {
					HeapFile heapFile = null;
					BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					String csvSchema = br.readLine();
					heapFile = new HeapFile(query.getHeapFilePath(), csvSchema);
					if(Main.VERBOSE) System.out.println(heapFile);
					if(query.getHashColumnIds() != null && query.getHashColumnIds().size() > 0){
						try {
							heapFile.addHashColumnIdToHeader(query.getHashColumnIds());
							heapFile.buildHashOnColumn(query.getHashColumnIds());
						} catch (HashException e) {
							//e.printStackTrace();
						}
					}
					String rec = null;
					while((rec = br.readLine()) != null) {
						heapFile.addRecord(rec);
					}
				} catch (SchemaException e) {
					e.printStackTrace();
				}
			} else {
				try {
					HeapFile heapFile = null;
					File csvFile = new File(query.getImportFilePath());
					BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(csvFile)));
					String csvSchema = br.readLine();
					heapFile = new HeapFile(query.getHeapFilePath(), csvSchema);
					if(Main.VERBOSE) System.out.println(heapFile);
					if(query.getHashColumnIds() != null && query.getHashColumnIds().size() > 0){
						try {
							heapFile.addHashColumnIdToHeader(query.getHashColumnIds());
							heapFile.buildHashOnColumn(query.getHashColumnIds());
						} catch (HashException e) {
							//e.printStackTrace();
						}
					}
					String rec = null;
					int index = 0;
					while((rec = br.readLine()) != null) {
						if(Main.VERBOSE && (index%100 == 0)) System.out.println("Inserting Record " + index);
						heapFile.addRecord(rec);
						index++;
					}
				} catch (SchemaException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 
	}
	
	private void executeSelectQuery(QuerySelect query) {
		HeapFile heapFile = null;
		heapFile = new HeapFile(query.getHeapFilePath());
		if(Main.VERBOSE) System.out.println(" -- " + heapFile);
		Cursor cursor = heapFile.getQualifiyingRecords(query.getEqualityConditions(), query.getQueryConditions(), query.getProjections());
		if(query.getExportFilePath()!=null)
		{
			try {
				File csvFile = CSVFileHelper.createCSVFile(query.getExportFilePath());
				CSVFileHelper.addRecords(csvFile, heapFile, cursor);
			} catch (InvalidFileException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			if(Main.VERBOSE) System.out.println("::: Query Output ::: " + cursor.getNumRecords());
			System.out.println(cursor.getProjectedSchema());
			Iterator<Long> iter = cursor.iterator();
			Long recID = null;
			Set<Integer> projectionIndexes = cursor.getProjectionIndexes();
            while(iter.hasNext())
            {
            	recID = iter.next();
            	System.out.println(heapFile.getCursorRecord(recID, projectionIndexes).getRecordAsString(false));
            }
            if(Main.VERBOSE) System.out.println("::: END of Query Output ::: ");
		}
	}
	
	private void executeBuildHashQuery(QueryBuildHash query) {
		HeapFile heapFile = new HeapFile(query.getHeapFilePath());
		try {
			heapFile.addHashColumnIdToHeader(query.getColumnIds());
			heapFile.buildHashOnColumn(query.getColumnIds());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (HashException e) {
			//e.printStackTrace();
		}
	}
	
}
