package dbsi;


public class Main {

	public static final Boolean IS_WINDOWS = true; 
	public static final Boolean VERBOSE = false;
	public static final Boolean DEBUG = false;
	public static final int READ_LIMIT = 100;
	
	public static void main(String[] args) {
		DataBase db = new DataBase();		
		db.executeParams(args);
		
//		testing(db);
	}

//	private static void testing(DataBase db) {		
//		Long start = System.currentTimeMillis();
//		final String filename = "zz";
//		
//		deleteFiles(filename);
//		db.executeParams("files/zz.hf -i < files/zz.csv".split(" "));
//		db.executeParams("files/zz.hf".split(" "));
//		start = System.currentTimeMillis();
//		db.executeParams("files/zz.hf -s1 = -36733650".split(" ")); 
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s2 = 0.7272453921452413".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s3 = 0.3416314125061035".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s4 = F12".split(" ")); 
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s5 = L56".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s6 = 12".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		printTime(start);
//		db.executeParams("files/zz.hf -b1 -b2 -b3 -b4 -b5 -b6".split(" "));
//		start = System.currentTimeMillis();
//		db.executeParams("files/zz.hf -s1 = -36733650".split(" ")); 
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s2 = 0.7272453921452413".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s3 = 0.3416314125061035".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s4 = F12".split(" ")); 
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s5 = L56".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		db.executeParams("files/zz.hf -s6 = 12".split(" "));
//		System.out.println(":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
//		printTime(start);
//	}
//	
//	private static void printTime(Long start) {
//		Long stop = System.currentTimeMillis();		
//		System.out.println("TOTAL EXECUTION TIME :: " + (stop - start) / 1000 + "secs (" + (stop - start) + "millisecs)");
//	}
//
//	private static void deleteFiles(String filename) {
//		final String f = filename;
//		File filesDir = new File("files/");
//		FilenameFilter filter = new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				boolean fileOK = true;
//			    if (name != null) {
//			      fileOK &= name.startsWith(f + ".h");
//			    }
//			    return fileOK;
//			}
//		};
//		File[] filteredFiles = filesDir.listFiles(filter);
//		for (File file : filteredFiles) {
//			//System.out.println(" deleting -- " + file.getAbsolutePath());
//			file.delete();
//		}
//	}
	
}
