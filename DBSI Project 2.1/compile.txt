Name: 
Kannapiran Thirunarayanan (kt2424)
Sandeep Renganathan (sr2962)
-------------------------------------------------------------------------------------
## remove all .class from subdirectories: 
## (go to the "dbsi_project" folder and do)
find . -name "*.class" | xargs rm -rf
-------------------------------------------------------------------------------------
## to compile all java files:
## go to the location were the project is placed
cd dbsi_project

## now you can see two folders: 
## 1. "dbsi"(which contains src files, dbsi is a package name),
## 2. "bin"(which will contain the class files)
javac -sourcepath src src/dbsi/Main.java -d bin

## now all the .class will be generated in the bin folder.

## to run Main program
java -cp bin dbsi.Main <Arguments>

## the program will be run and output will be displayed
## on screen or written to a file.
-------------------------------------------------------------------------------------
Important Notes: 
We have fixed our earlier bug of having to include < or > for input/output csv files. As a result now the normal terminal input/output will work fine (eg. java -cp bin dbsi.Main ./heap -i < ./sample_insert_1.csv) and (eg. java -cp bin dbsi.Main ./heap > output.csv).
But, while giving < or > as conditional operators it still has to be given inside double quotes as before (eg. java -cp bin dbsi.Main ./heap -s1 ">=" "Nicole" -s3 ">" 1924 -p1 -p2 > output.csv)

-------------------------------------------------------------------------------------
Statistics: (the csv file used has 100,000 records and is included with our submission zh.csv)

1. Time Taken:
We tested out program with input file containing 100,000 records. We ran the same set of select querires before and after building the index on columns. 
The following are the results:

Without HASH: 
select query on column (type: c5) took 346 milliseconds
select query on column (type: i4) took 420 milliseconds

With HASH:
select query on column (type: c5) took 120 milliseconds
select query on column (type: i4) took 250 milliseconds
 
2. RECORDS_PER_BUCKET: 
Since we found out that using one record per bucket in the HASH table would give us the least Hash and Overflow file size, we decided to use it. 
But, as a result this change increased the amount of time needed to build the hash index because the number of splits would be huge. 
Given below are the test results for why we decided to use RECORDS_PER_BUKET as 1 (all result corresping to building hash on an input file with 100,000 records). 

#rec_per_bucket   time_taken   hash_file_size   overflow_file_size
1                 1103 sec     2 MB             2 MB
2                 564 sec      3.2 MB           1.6 MB
5                 275 sec      6.8 MB           1.4 MB
10                169 sec      12.7 MB          1.3 MB
20                138 sec      24.3 MB          1.2 MB      
-------------------------------------------------------------------------------------
Sample Test Cases that worked for us: 

java -cp bin dbsi.Main ./heap -i < ./sample_insert_1.csv
java -cp bin dbsi.Main ./heap -i -b1 -b4 < ./sample_insert_1.csv
java -cp bin dbsi.Main ./heap -b2 -b2
java -cp bin dbsi.Main ./heap > ./out.csv
java -cp bin dbsi.Main ./heap -s1 ">=" "Nicole" -s3 ">=" 1924 -p1 -p2 > ./out.csv 
java -cp bin dbsi.Main ./heap -s1 "=" "Nicole" -s1 "=" "Orson" -p1 -p2 > ./out.csv 
java -cp bin dbsi.Main ./heap -p1 -p2
java -cp bin dbsi.Main ./heap 